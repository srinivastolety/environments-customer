nohup: ignoring input
[DEPRECATION WARNING]: The use of 'include' for tasks has been deprecated. Use 
'import_tasks' for static inclusions or 'include_tasks' for dynamic inclusions.
 This feature will be removed in a future release. Deprecation warnings can be 
disabled by setting deprecation_warnings=False in ansible.cfg.
[DEPRECATION WARNING]: include is kept for backwards compatibility but usage is
 discouraged. The module documentation details page may explain more about this
 rationale.. This feature will be removed in a future release. Deprecation 
warnings can be disabled by setting deprecation_warnings=False in ansible.cfg.
[DEPRECATION WARNING]: The use of 'static' has been deprecated. Use 
'import_tasks' for static inclusion, or 'include_tasks' for dynamic inclusion. 
This feature will be removed in a future release. Deprecation warnings can be 
disabled by setting deprecation_warnings=False in ansible.cfg.
 [WARNING]: While constructing a mapping from /home/ansible/hebb/installer
/third-party/ansible-oracle/roles/orahost/tasks/main.yml, line 6, column 7,
found a duplicate dict key (that). Using last defined value only.
[DEPRECATION WARNING]: The comma separated role spec format, use the 
yaml/explicit format instead. Line that trigger this: he-win-config-c-drive,. 
This feature will be removed in version 2.7. Deprecation warnings can be 
disabled by setting deprecation_warnings=False in ansible.cfg.
 [WARNING]: Could not match supplied host pattern, ignoring: windows-servers

PLAY [Load up meta-data and ansible password for windows] **********************
skipping: no hosts matched

PLAY [Make sure servers are up] ************************************************

TASK [Wait for hosts] **********************************************************
Friday 03 May 2019  13:17:00 -0400 (0:00:00.050)       0:00:00.050 ************ 
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]
ok: [bcne-lb-pf01]
ok: [bcne-con-pf02]
ok: [bcne-con-pf01]

TASK [Main he-load-metadata] ***************************************************
Friday 03 May 2019  13:17:01 -0400 (0:00:01.141)       0:00:01.192 ************ 

TASK [he-load-metadata : Do we have a override file for this environment?] *****
Friday 03 May 2019  13:17:01 -0400 (0:00:00.254)       0:00:01.447 ************ 
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Test environment override file] ***********************
Friday 03 May 2019  13:17:05 -0400 (0:00:04.119)       0:00:05.566 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-con-pf01]
skipping: [bcne-lb-pf01]

TASK [he-load-metadata : Include_vars environment overrides file] **************
Friday 03 May 2019  13:17:05 -0400 (0:00:00.171)       0:00:05.738 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Do we have a default customer.yml for this customer?] ***
Friday 03 May 2019  13:17:06 -0400 (0:00:00.267)       0:00:06.006 ************ 
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-lb-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]

TASK [he-load-metadata : Test customer.yml] ************************************
Friday 03 May 2019  13:17:09 -0400 (0:00:03.972)       0:00:09.978 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-load-metadata : Include_vars customer.yml] ****************************
Friday 03 May 2019  13:17:10 -0400 (0:00:00.141)       0:00:10.120 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Do we have an SLA-specific customer-<SLA>.yml for this customer?] ***
Friday 03 May 2019  13:17:10 -0400 (0:00:00.304)       0:00:10.425 ************ 
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include SLA-Specific customer.yml] ********************
Friday 03 May 2019  13:17:14 -0400 (0:00:03.863)       0:00:14.288 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Do we have a diff-by-sla file for this SLA?] **********
Friday 03 May 2019  13:17:14 -0400 (0:00:00.275)       0:00:14.564 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-lb-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]

TASK [he-load-metadata : Include default diff-by-sla meta-data] ****************
Friday 03 May 2019  13:17:15 -0400 (0:00:01.365)       0:00:15.930 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include SLA-specific diff-by-sla meta-data] ***********
Friday 03 May 2019  13:17:16 -0400 (0:00:00.288)       0:00:16.218 ************ 
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-load-metadata : Do we have a defaults.yml in the metadata locale ?] ***
Friday 03 May 2019  13:17:16 -0400 (0:00:00.215)       0:00:16.434 ************ 
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-lb-pf01 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-con-pf01 -> localhost]

TASK [he-load-metadata : Include metadata defaults.yml] ************************
Friday 03 May 2019  13:17:20 -0400 (0:00:04.312)       0:00:20.747 ************ 
 [WARNING]: While constructing a mapping from True, line 4, column 1, found a
duplicate dict key (caremanager_template_name). Using last defined value only.
 [WARNING]: While constructing a mapping from True, line 4, column 1, found a
duplicate dict key (caremanager_template_name). Using last defined value only.
ok: [bcne-wl-pf01 -> localhost]
 [WARNING]: While constructing a mapping from True, line 4, column 1, found a
duplicate dict key (caremanager_template_name). Using last defined value only.
 [WARNING]: While constructing a mapping from True, line 4, column 1, found a
duplicate dict key (caremanager_template_name). Using last defined value only.
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
 [WARNING]: While constructing a mapping from True, line 4, column 1, found a
duplicate dict key (caremanager_template_name). Using last defined value only.
ok: [bcne-wl-pf03 -> localhost]
 [WARNING]: While constructing a mapping from True, line 4, column 1, found a
duplicate dict key (caremanager_template_name). Using last defined value only.
 [WARNING]: While constructing a mapping from True, line 4, column 1, found a
duplicate dict key (caremanager_template_name). Using last defined value only.
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
 [WARNING]: While constructing a mapping from True, line 4, column 1, found a
duplicate dict key (caremanager_template_name). Using last defined value only.
ok: [bcne-con-pf02 -> localhost]
 [WARNING]: While constructing a mapping from True, line 4, column 1, found a
duplicate dict key (caremanager_template_name). Using last defined value only.
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include_vars environment overrides file to key into provisioner region] ***
Friday 03 May 2019  13:17:21 -0400 (0:00:00.301)       0:00:21.049 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]
ok: [bcne-con-pf01 -> localhost]

TASK [he-load-metadata : Include Provisioner-region metadata] ******************
Friday 03 May 2019  13:17:21 -0400 (0:00:00.288)       0:00:21.338 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include_vars environment overrides file to key into provisioner region] ***
Friday 03 May 2019  13:17:21 -0400 (0:00:00.247)       0:00:21.586 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include base secrets file] ****************************
Friday 03 May 2019  13:17:21 -0400 (0:00:00.305)       0:00:21.891 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include provisioner secrets file] *********************
Friday 03 May 2019  13:17:22 -0400 (0:00:00.275)       0:00:22.167 ************ 
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Do we have region specific secrets for this provisioner ?] ***
Friday 03 May 2019  13:17:22 -0400 (0:00:00.291)       0:00:22.458 ************ 
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include region specific secrets file] *****************
Friday 03 May 2019  13:17:23 -0400 (0:00:01.381)       0:00:23.840 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-load-metadata : Do we have generic customer secrets file?] ************
Friday 03 May 2019  13:17:24 -0400 (0:00:00.164)       0:00:24.004 ************ 
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include generic customer secrets file] ****************
Friday 03 May 2019  13:17:27 -0400 (0:00:03.939)       0:00:27.943 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-lb-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]

TASK [he-load-metadata : Do we have customer-environment specific secrets ?] ***
Friday 03 May 2019  13:17:28 -0400 (0:00:00.238)       0:00:28.182 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include customer-environment specific secrets] ********
Friday 03 May 2019  13:17:32 -0400 (0:00:03.931)       0:00:32.113 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include_vars environment overrides file one last time to catch overrides] ***
Friday 03 May 2019  13:17:32 -0400 (0:00:00.324)       0:00:32.437 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Do we have a default customer.yml for this customer?] ***
Friday 03 May 2019  13:17:32 -0400 (0:00:00.218)       0:00:32.656 ************ 
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Test customer.yml] ************************************
Friday 03 May 2019  13:17:36 -0400 (0:00:04.119)       0:00:36.776 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-load-metadata : Include_vars customer.yml] ****************************
Friday 03 May 2019  13:17:36 -0400 (0:00:00.191)       0:00:36.967 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Do we have an SLA-specific customer-<SLA>.yml for this customer?] ***
Friday 03 May 2019  13:17:37 -0400 (0:00:00.255)       0:00:37.223 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include SLA-Specific customer.yml] ********************
Friday 03 May 2019  13:17:41 -0400 (0:00:03.980)       0:00:41.203 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

TASK [he-load-metadata : Include_vars environment overrides file one last time to catch overrides] ***
Friday 03 May 2019  13:17:41 -0400 (0:00:00.322)       0:00:41.526 ************ 
ok: [bcne-wl-pf01 -> localhost]
ok: [bcne-wl-pf03 -> localhost]
ok: [bcne-wl-pf02 -> localhost]
ok: [bcne-wl-pf04 -> localhost]
ok: [bcne-wl-pf05 -> localhost]
ok: [bcne-wl-pf06 -> localhost]
ok: [bcne-con-pf01 -> localhost]
ok: [bcne-con-pf02 -> localhost]
ok: [bcne-lb-pf01 -> localhost]

PLAY [Create OS specific groups] ***********************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:17:41 -0400 (0:00:00.336)       0:00:41.862 ************ 
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf01]
ok: [bcne-con-pf01]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]

TASK [Create OS based groups] **************************************************
Friday 03 May 2019  13:17:43 -0400 (0:00:01.671)       0:00:43.533 ************ 
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf01]
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]

PLAY [Configure CentOS] ********************************************************

TASK [he-run-level : Get systemctl default] ************************************
Friday 03 May 2019  13:17:43 -0400 (0:00:00.372)       0:00:43.906 ************ 
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf02]
ok: [bcne-con-pf02]
ok: [bcne-wl-pf06]
ok: [bcne-lb-pf01]
ok: [bcne-con-pf01]

TASK [he-run-level : Switch to requested systemctl default] ********************
Friday 03 May 2019  13:17:45 -0400 (0:00:01.135)       0:00:45.042 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-run-level : Is graphical currently active?] ***************************
Friday 03 May 2019  13:17:45 -0400 (0:00:00.157)       0:00:45.200 ************ 
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf01]
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]

TASK [he-run-level : Switch to graphical] **************************************
Friday 03 May 2019  13:17:46 -0400 (0:00:01.006)       0:00:46.206 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-run-level : Is multi-user currently active?] **************************
Friday 03 May 2019  13:17:46 -0400 (0:00:00.239)       0:00:46.445 ************ 
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf01]
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]

TASK [he-run-level : Switch to multi-user] *************************************
Friday 03 May 2019  13:17:47 -0400 (0:00:00.904)       0:00:47.349 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : AWS: Put swap in fstab.] **********************************
Friday 03 May 2019  13:17:47 -0400 (0:00:00.160)       0:00:47.510 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : AWS: Enable swap.] ****************************************
Friday 03 May 2019  13:17:47 -0400 (0:00:00.172)       0:00:47.683 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Get list of current swap parition] ************************
Friday 03 May 2019  13:17:47 -0400 (0:00:00.245)       0:00:47.928 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:48 -0400 (0:00:00.173)       0:00:48.102 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]
skipping: [bcne-con-pf01]

TASK [he-swap-disk : Disable existing swap disk] *******************************
Friday 03 May 2019  13:17:48 -0400 (0:00:00.251)       0:00:48.354 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:48 -0400 (0:00:00.199)       0:00:48.553 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf04]
skipping: [bcne-con-pf01]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Get swapon status] ****************************************
Friday 03 May 2019  13:17:48 -0400 (0:00:00.225)       0:00:48.779 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:48 -0400 (0:00:00.220)       0:00:48.999 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Get existing /etc/fstab] **********************************
Friday 03 May 2019  13:17:49 -0400 (0:00:00.188)       0:00:49.188 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:49 -0400 (0:00:00.252)       0:00:49.441 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Remove /etc/fstab swap entry] *****************************
Friday 03 May 2019  13:17:49 -0400 (0:00:00.183)       0:00:49.624 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-lb-pf01]
skipping: [bcne-con-pf02]

TASK [he-swap-disk : Get /etc/fstab after change] ******************************
Friday 03 May 2019  13:17:49 -0400 (0:00:00.271)       0:00:49.896 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:50 -0400 (0:00:00.177)       0:00:50.073 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Calculate swap in KB] *************************************
Friday 03 May 2019  13:17:50 -0400 (0:00:00.284)       0:00:50.357 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Get current swap partition sizes in Kb from swapon -s] ****
Friday 03 May 2019  13:17:50 -0400 (0:00:00.174)       0:00:50.532 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Calculate Swap to Add] ************************************
Friday 03 May 2019  13:17:50 -0400 (0:00:00.204)       0:00:50.736 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:50 -0400 (0:00:00.234)       0:00:50.970 ************ 
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:51 -0400 (0:00:00.167)       0:00:51.138 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-con-pf01]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:51 -0400 (0:00:00.266)       0:00:51.405 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Determine if we've created our new parition] **************
Friday 03 May 2019  13:17:51 -0400 (0:00:00.169)       0:00:51.574 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:51 -0400 (0:00:00.249)       0:00:51.824 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Create partition w/ FDisk] ********************************
Friday 03 May 2019  13:17:52 -0400 (0:00:00.195)       0:00:52.020 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:52 -0400 (0:00:00.185)       0:00:52.205 ************ 
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Fail if fdisk reports no free sectors or value out of range] ***
Friday 03 May 2019  13:17:52 -0400 (0:00:00.239)       0:00:52.445 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : List the partition table] *********************************
Friday 03 May 2019  13:17:52 -0400 (0:00:00.169)       0:00:52.614 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:52 -0400 (0:00:00.269)       0:00:52.883 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Use partprobe for the kernel to see the newly created partition] ***
Friday 03 May 2019  13:17:53 -0400 (0:00:00.175)       0:00:53.058 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-lb-pf01]
skipping: [bcne-con-pf02]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:53 -0400 (0:00:00.211)       0:00:53.270 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Get partprobe summary] ************************************
Friday 03 May 2019  13:17:53 -0400 (0:00:00.218)       0:00:53.488 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:53 -0400 (0:00:00.169)       0:00:53.657 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Determine if we've previously run mkswap] *****************
Friday 03 May 2019  13:17:53 -0400 (0:00:00.262)       0:00:53.919 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:54 -0400 (0:00:00.167)       0:00:54.087 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-lb-pf01]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]

TASK [he-swap-disk : Mkswap, set our new partition as a swap area] *************
Friday 03 May 2019  13:17:54 -0400 (0:00:00.250)       0:00:54.337 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:54 -0400 (0:00:00.179)       0:00:54.516 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-lb-pf01]
skipping: [bcne-con-pf02]

TASK [he-swap-disk : Enable new partition as swap] *****************************
Friday 03 May 2019  13:17:54 -0400 (0:00:00.179)       0:00:54.696 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:54 -0400 (0:00:00.242)       0:00:54.938 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Get swapon status] ****************************************
Friday 03 May 2019  13:17:55 -0400 (0:00:00.183)       0:00:55.121 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf04]
skipping: [bcne-con-pf01]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:55 -0400 (0:00:00.257)       0:00:55.379 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Make /etc/fstab file entry] *******************************
Friday 03 May 2019  13:17:55 -0400 (0:00:00.165)       0:00:55.544 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Calculate swap in KB] *************************************
Friday 03 May 2019  13:17:55 -0400 (0:00:00.245)       0:00:55.790 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Get current swap partition sizes in Kb from swapon -s] ****
Friday 03 May 2019  13:17:55 -0400 (0:00:00.201)       0:00:55.991 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Calculate Swap to Add] ************************************
Friday 03 May 2019  13:17:56 -0400 (0:00:00.161)       0:00:56.153 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:56 -0400 (0:00:00.257)       0:00:56.411 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:56 -0400 (0:00:00.230)       0:00:56.641 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:56 -0400 (0:00:00.303)       0:00:56.945 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Determine if we've created our new parition (sda3)] *******
Friday 03 May 2019  13:17:57 -0400 (0:00:00.157)       0:00:57.102 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-lb-pf01]
skipping: [bcne-con-pf02]

TASK [he-swap-disk : Create partition w/ FDisk] ********************************
Friday 03 May 2019  13:17:57 -0400 (0:00:00.232)       0:00:57.335 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:57 -0400 (0:00:00.167)       0:00:57.502 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Use partx to notify kernel of partition table changes (may show up as changed but idempotent)] ***
Friday 03 May 2019  13:17:57 -0400 (0:00:00.157)       0:00:57.660 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:57 -0400 (0:00:00.244)       0:00:57.905 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Mkswap, set our new partition as a swap area] *************
Friday 03 May 2019  13:17:58 -0400 (0:00:00.158)       0:00:58.063 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:58 -0400 (0:00:00.207)       0:00:58.270 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Determine if we've enabled our swap parition] *************
Friday 03 May 2019  13:17:58 -0400 (0:00:00.198)       0:00:58.469 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Enable new partition as swap] *****************************
Friday 03 May 2019  13:17:58 -0400 (0:00:00.155)       0:00:58.624 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : debug] ****************************************************
Friday 03 May 2019  13:17:58 -0400 (0:00:00.254)       0:00:58.879 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-swap-disk : Make /etc/fstab file entry] *******************************
Friday 03 May 2019  13:17:59 -0400 (0:00:00.169)       0:00:59.048 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-configure-etc-hosts : fail] *******************************************
Friday 03 May 2019  13:17:59 -0400 (0:00:00.185)       0:00:59.234 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-configure-etc-hosts : fail] *******************************************
Friday 03 May 2019  13:17:59 -0400 (0:00:00.218)       0:00:59.453 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-configure-etc-hosts : Setting domain name Datacenter for Savvis] ******
Friday 03 May 2019  13:17:59 -0400 (0:00:00.161)       0:00:59.614 ************ 
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf04]
skipping: [bcne-con-pf02]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-lb-pf01]

TASK [he-configure-etc-hosts : Setting domain name Datacenter for NaviSite] ****
Friday 03 May 2019  13:17:59 -0400 (0:00:00.240)       0:00:59.855 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-configure-etc-hosts : Configure /etc/hosts on the target machine] *****
Friday 03 May 2019  13:18:00 -0400 (0:00:00.174)       0:01:00.029 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-lb-pf01]
skipping: [bcne-con-pf02]

TASK [he-ad-join-centos : VMware Upload the current AD domain join / Unix host setup script] ***
Friday 03 May 2019  13:18:00 -0400 (0:00:00.186)       0:01:00.215 ************ 
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf01]
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]

TASK [he-ad-join-centos : VMware Execute the current AD domain join / Unix host setup script] ***
Friday 03 May 2019  13:18:02 -0400 (0:00:01.874)       0:01:02.089 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf06]
changed: [bcne-con-pf01]
changed: [bcne-lb-pf01]
changed: [bcne-con-pf02]

PLAY [CMDB debug] **************************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:18:03 -0400 (0:00:01.470)       0:01:03.559 ************ 
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf02]
ok: [bcne-con-pf01]
ok: [bcne-lb-pf01]

TASK [he-debug-ops-utils-cmdb-serverline : debug] ******************************
Friday 03 May 2019  13:18:05 -0400 (0:00:01.854)       0:01:05.414 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-debug-ops-utils-cmdb-serverline : include] ****************************
Friday 03 May 2019  13:18:06 -0400 (0:00:00.742)       0:01:06.156 ************ 
included: /home/ansible/hebb/installer/common/roles/he-debug-ops-utils-cmdb-serverline/tasks/print-debug-lines.yml for bcne-wl-pf02, bcne-wl-pf01, bcne-wl-pf03, bcne-wl-pf04, bcne-wl-pf05, bcne-wl-pf06, bcne-con-pf01, bcne-con-pf02, bcne-lb-pf01

TASK [he-debug-ops-utils-cmdb-serverline : debug cmdb_server_role] *************
Friday 03 May 2019  13:18:06 -0400 (0:00:00.388)       0:01:06.544 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "cmdb_server_role: hrMultiServerPrimary"
}
ok: [bcne-wl-pf02] => {
    "msg": "cmdb_server_role: hrMultiServerSecondary"
}
ok: [bcne-wl-pf03] => {
    "msg": "cmdb_server_role: hrMultiServerSecondary"
}
ok: [bcne-wl-pf04] => {
    "msg": "cmdb_server_role: hrMultiServerSecondary"
}
ok: [bcne-wl-pf05] => {
    "msg": "cmdb_server_role: hrMultiServerSecondary"
}
ok: [bcne-con-pf01] => {
    "msg": "cmdb_server_role: cprime"
}
ok: [bcne-wl-pf06] => {
    "msg": "cmdb_server_role: hrMultiServerSecondary"
}
ok: [bcne-con-pf02] => {
    "msg": "cmdb_server_role: cprime"
}
ok: [bcne-lb-pf01] => {
    "msg": "cmdb_server_role: apacheLb"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug sys_app_user] *****************
Friday 03 May 2019  13:18:06 -0400 (0:00:00.380)       0:01:06.925 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "sys_app_user: weblogic"
}
ok: [bcne-wl-pf03] => {
    "msg": "sys_app_user: weblogic"
}
ok: [bcne-wl-pf02] => {
    "msg": "sys_app_user: weblogic"
}
ok: [bcne-con-pf02] => {
    "msg": "sys_app_user: cprime"
}
ok: [bcne-wl-pf04] => {
    "msg": "sys_app_user: weblogic"
}
ok: [bcne-wl-pf05] => {
    "msg": "sys_app_user: weblogic"
}
ok: [bcne-wl-pf06] => {
    "msg": "sys_app_user: weblogic"
}
ok: [bcne-con-pf01] => {
    "msg": "sys_app_user: cprime"
}
ok: [bcne-lb-pf01] => {
    "msg": "sys_app_user: httpd"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug ansible_hostname] *************
Friday 03 May 2019  13:18:07 -0400 (0:00:00.335)       0:01:07.260 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ansible_hostname: bcne-wl-pf01"
}
ok: [bcne-wl-pf02] => {
    "msg": "ansible_hostname: bcne-wl-pf02"
}
ok: [bcne-wl-pf03] => {
    "msg": "ansible_hostname: bcne-wl-pf03"
}
ok: [bcne-wl-pf04] => {
    "msg": "ansible_hostname: bcne-wl-pf04"
}
ok: [bcne-wl-pf06] => {
    "msg": "ansible_hostname: bcne-wl-pf06"
}
ok: [bcne-wl-pf05] => {
    "msg": "ansible_hostname: bcne-wl-pf05"
}
ok: [bcne-con-pf01] => {
    "msg": "ansible_hostname: bcne-con-pf01"
}
ok: [bcne-lb-pf01] => {
    "msg": "ansible_hostname: bcne-lb-pf01"
}
ok: [bcne-con-pf02] => {
    "msg": "ansible_hostname: bcne-con-pf02"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug ansible_default_ipv4] *********
Friday 03 May 2019  13:18:07 -0400 (0:00:00.341)       0:01:07.602 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ansible_default_ipv4: {u'macaddress': u'00:50:56:82:a2:71', u'network': u'172.29.15.128', u'mtu': 1500, u'broadcast': u'172.29.15.255', u'alias': u'ens192', u'netmask': u'255.255.255.128', u'address': u'172.29.15.190', u'interface': u'ens192', u'type': u'ether', u'gateway': u'172.29.15.129'}"
}
ok: [bcne-wl-pf03] => {
    "msg": "ansible_default_ipv4: {u'macaddress': u'00:50:56:82:c7:f2', u'network': u'172.29.15.128', u'mtu': 1500, u'broadcast': u'172.29.15.255', u'alias': u'ens192', u'netmask': u'255.255.255.128', u'address': u'172.29.15.192', u'interface': u'ens192', u'type': u'ether', u'gateway': u'172.29.15.129'}"
}
ok: [bcne-wl-pf02] => {
    "msg": "ansible_default_ipv4: {u'macaddress': u'00:50:56:82:c0:a7', u'network': u'172.29.15.128', u'mtu': 1500, u'broadcast': u'172.29.15.255', u'alias': u'ens192', u'netmask': u'255.255.255.128', u'address': u'172.29.15.191', u'interface': u'ens192', u'type': u'ether', u'gateway': u'172.29.15.129'}"
}
ok: [bcne-wl-pf04] => {
    "msg": "ansible_default_ipv4: {u'macaddress': u'00:50:56:82:7d:8c', u'network': u'172.29.15.128', u'mtu': 1500, u'broadcast': u'172.29.15.255', u'alias': u'ens192', u'netmask': u'255.255.255.128', u'address': u'172.29.15.193', u'interface': u'ens192', u'type': u'ether', u'gateway': u'172.29.15.129'}"
}
ok: [bcne-wl-pf05] => {
    "msg": "ansible_default_ipv4: {u'macaddress': u'00:50:56:82:45:62', u'network': u'172.29.15.128', u'mtu': 1500, u'broadcast': u'172.29.15.255', u'alias': u'ens192', u'netmask': u'255.255.255.128', u'address': u'172.29.15.194', u'interface': u'ens192', u'type': u'ether', u'gateway': u'172.29.15.129'}"
}
ok: [bcne-con-pf02] => {
    "msg": "ansible_default_ipv4: {u'macaddress': u'00:50:56:82:f5:87', u'network': u'172.29.15.128', u'mtu': 1500, u'broadcast': u'172.29.15.255', u'alias': u'ens192', u'netmask': u'255.255.255.128', u'address': u'172.29.15.197', u'interface': u'ens192', u'type': u'ether', u'gateway': u'172.29.15.129'}"
}
ok: [bcne-wl-pf06] => {
    "msg": "ansible_default_ipv4: {u'macaddress': u'00:50:56:82:d3:40', u'network': u'172.29.15.128', u'mtu': 1500, u'broadcast': u'172.29.15.255', u'alias': u'ens192', u'netmask': u'255.255.255.128', u'address': u'172.29.15.195', u'interface': u'ens192', u'type': u'ether', u'gateway': u'172.29.15.129'}"
}
ok: [bcne-con-pf01] => {
    "msg": "ansible_default_ipv4: {u'macaddress': u'00:50:56:82:e5:0b', u'network': u'172.29.15.128', u'mtu': 1500, u'broadcast': u'172.29.15.255', u'alias': u'ens192', u'netmask': u'255.255.255.128', u'address': u'172.29.15.196', u'interface': u'ens192', u'type': u'ether', u'gateway': u'172.29.15.129'}"
}
ok: [bcne-lb-pf01] => {
    "msg": "ansible_default_ipv4: {u'macaddress': u'00:50:56:82:29:65', u'network': u'172.29.15.128', u'mtu': 1500, u'broadcast': u'172.29.15.255', u'alias': u'ens192', u'netmask': u'255.255.255.128', u'address': u'172.29.15.198', u'interface': u'ens192', u'type': u'ether', u'gateway': u'172.29.15.129'}"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug env_type] *********************
Friday 03 May 2019  13:18:07 -0400 (0:00:00.391)       0:01:07.993 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "env_type: performance"
}
ok: [bcne-wl-pf03] => {
    "msg": "env_type: performance"
}
ok: [bcne-wl-pf02] => {
    "msg": "env_type: performance"
}
ok: [bcne-wl-pf04] => {
    "msg": "env_type: performance"
}
ok: [bcne-wl-pf05] => {
    "msg": "env_type: performance"
}
ok: [bcne-lb-pf01] => {
    "msg": "env_type: performance"
}
ok: [bcne-wl-pf06] => {
    "msg": "env_type: performance"
}
ok: [bcne-con-pf02] => {
    "msg": "env_type: performance"
}
ok: [bcne-con-pf01] => {
    "msg": "env_type: performance"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug env_status] *******************
Friday 03 May 2019  13:18:08 -0400 (0:00:00.369)       0:01:08.363 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "env_status: live"
}
ok: [bcne-wl-pf03] => {
    "msg": "env_status: live"
}
ok: [bcne-wl-pf02] => {
    "msg": "env_status: live"
}
ok: [bcne-wl-pf04] => {
    "msg": "env_status: live"
}
ok: [bcne-wl-pf05] => {
    "msg": "env_status: live"
}
ok: [bcne-con-pf01] => {
    "msg": "env_status: live"
}
ok: [bcne-wl-pf06] => {
    "msg": "env_status: live"
}
ok: [bcne-con-pf02] => {
    "msg": "env_status: live"
}
ok: [bcne-lb-pf01] => {
    "msg": "env_status: live"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug env_status] *******************
Friday 03 May 2019  13:18:08 -0400 (0:00:00.287)       0:01:08.651 ************ 
ok: [bcne-wl-pf02] => {
    "msg": "env_status: live"
}
ok: [bcne-wl-pf01] => {
    "msg": "env_status: live"
}
ok: [bcne-wl-pf04] => {
    "msg": "env_status: live"
}
ok: [bcne-wl-pf03] => {
    "msg": "env_status: live"
}
ok: [bcne-wl-pf05] => {
    "msg": "env_status: live"
}
ok: [bcne-wl-pf06] => {
    "msg": "env_status: live"
}
ok: [bcne-con-pf01] => {
    "msg": "env_status: live"
}
ok: [bcne-con-pf02] => {
    "msg": "env_status: live"
}
ok: [bcne-lb-pf01] => {
    "msg": "env_status: live"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug server] ***********************
Friday 03 May 2019  13:18:09 -0400 (0:00:00.372)       0:01:09.023 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.server: bcne-wl-pf01"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.server: bcne-wl-pf03"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.server: bcne-wl-pf05"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.server: bcne-wl-pf02"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.server: bcne-wl-pf04"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.server: bcne-wl-pf06"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.server: bcne-con-pf01"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.server: bcne-con-pf02"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.server: bcne-lb-pf01"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug ip] ***************************
Friday 03 May 2019  13:18:09 -0400 (0:00:00.410)       0:01:09.433 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.ip: 172.29.15.190"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.ip: 172.29.15.191"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.ip: 172.29.15.192"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.ip: 172.29.15.193"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.ip: 172.29.15.196"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.ip: 172.29.15.194"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.ip: 172.29.15.197"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.ip: 172.29.15.195"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.ip: 172.29.15.198"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug port] *************************
Friday 03 May 2019  13:18:09 -0400 (0:00:00.402)       0:01:09.836 ************ 
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.port: 22"
}
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.port: 22"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.port: 22"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.port: 22"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.port: 22"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.port: 22"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.port: 22"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.port: 22"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.port: 22"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug user] *************************
Friday 03 May 2019  13:18:10 -0400 (0:00:00.334)       0:01:10.170 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.user: weblogic"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.user: weblogic"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.user: weblogic"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.user: weblogic"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.user: weblogic"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.user: cprime"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.user: weblogic"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.user: cprime"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.user: httpd"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug customer] *********************
Friday 03 May 2019  13:18:10 -0400 (0:00:00.403)       0:01:10.573 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.customer: BCNE"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.customer: BCNE"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.customer: BCNE"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.customer: BCNE"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.customer: BCNE"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.customer: BCNE"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.customer: BCNE"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.customer: BCNE"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.customer: BCNE"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug env] **************************
Friday 03 May 2019  13:18:10 -0400 (0:00:00.404)       0:01:10.977 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.env: performance"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.env: performance"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.env: performance"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.env: performance"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.env: performance"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.env: performance"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.env: performance"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.env: performance"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.env: performance"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug Type] *************************
Friday 03 May 2019  13:18:11 -0400 (0:00:00.435)       0:01:11.413 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.Type: hrMultiServerPrimary"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.Type: hrMultiServerSecondary"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.Type: hrMultiServerSecondary"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.Type: hrMultiServerSecondary"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.Type: hrMultiServerSecondary"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.Type: hrMultiServerSecondary"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.Type: cprime"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.Type: cprime"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.Type: apacheLb"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug version] **********************
Friday 03 May 2019  13:18:11 -0400 (0:00:00.411)       0:01:11.824 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.version: 56"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.version: 56"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.version: 56"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.version: 56"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.version: 56"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.version: 56"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.version: 0"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.version: 0"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.version: 0"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug ARGUMENTS] ********************
Friday 03 May 2019  13:18:12 -0400 (0:00:00.306)       0:01:12.130 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.ARGUMENTS: generic,hrMultiServerPrimary"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.ARGUMENTS: generic,hrMultiServerSecondary"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.ARGUMENTS: generic,hrMultiServerSecondary"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.ARGUMENTS: generic,hrMultiServerSecondary"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.ARGUMENTS: generic,hrMultiServerSecondary"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.ARGUMENTS: generic,hrMultiServerSecondary"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.ARGUMENTS: generic,cprime"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.ARGUMENTS: generic,cprime"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.ARGUMENTS: generic,apacheLb"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug remote_ssh] *******************
Friday 03 May 2019  13:18:12 -0400 (0:00:00.401)       0:01:12.531 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.remote_ssh: 1"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.remote_ssh: 1"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.remote_ssh: 1"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.remote_ssh: 1"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.remote_ssh: 1"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.remote_ssh: 1"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.remote_ssh: 1"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.remote_ssh: 1"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.remote_ssh: 1"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug ssh_keys] *********************
Friday 03 May 2019  13:18:12 -0400 (0:00:00.400)       0:01:12.932 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.ssh_keys: 1"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.ssh_keys: 1"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.ssh_keys: 1"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.ssh_keys: 1"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.ssh_keys: 1"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.ssh_keys: 1"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.ssh_keys: 1"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.ssh_keys: 1"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.ssh_keys: 1"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug rsync] ************************
Friday 03 May 2019  13:18:13 -0400 (0:00:00.398)       0:01:13.330 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.rsync: 1"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.rsync: 1"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.rsync: 1"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.rsync: 1"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.rsync: 1"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.rsync: 1"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.rsync: 1"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.rsync: 1"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.rsync: 1"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug Keys] *************************
Friday 03 May 2019  13:18:13 -0400 (0:00:00.312)       0:01:13.642 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.Keys: 1"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.Keys: 1"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.Keys: 1"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.Keys: 1"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.Keys: 1"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.Keys: 1"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.Keys: 1"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.Keys: 1"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.Keys: 1"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug Comment] **********************
Friday 03 May 2019  13:18:14 -0400 (0:00:00.409)       0:01:14.052 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.Comment: "
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.Comment: "
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.Comment: "
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.Comment: "
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.Comment: "
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.Comment: "
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.Comment: "
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.Comment: "
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.Comment: "
}

TASK [he-debug-ops-utils-cmdb-serverline : debug New] **************************
Friday 03 May 2019  13:18:14 -0400 (0:00:00.401)       0:01:14.453 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.New: 1"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.New: 1"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.New: 1"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.New: 1"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.New: 1"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.New: 1"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.New: 1"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.New: 1"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.New: 1"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug MelissaPush] ******************
Friday 03 May 2019  13:18:14 -0400 (0:00:00.403)       0:01:14.857 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaPush: 1"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaPush: 1"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaPush: 1"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaPush: 1"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaPush: 1"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaPush: 0"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaPush: 1"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaPush: 0"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaPush: 0"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug MelissaUser] ******************
Friday 03 May 2019  13:18:15 -0400 (0:00:00.309)       0:01:15.166 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaUser: 1"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaUser: 1"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaUser: 1"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaUser: 1"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaUser: 1"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaUser: 1"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaUser: 0"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaUser: 0"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.MelissaUser: 0"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug SLA] **************************
Friday 03 May 2019  13:18:15 -0400 (0:00:00.393)       0:01:15.559 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.SLA: vranp"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.SLA: vranp"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.SLA: vranp"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.SLA: vranp"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.SLA: vranp"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.SLA: vranp"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.SLA: vranp"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.SLA: vranp"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.SLA: vranp"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug status] ***********************
Friday 03 May 2019  13:18:15 -0400 (0:00:00.400)       0:01:15.960 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.status: live"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.status: live"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields.status: live"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields.status: live"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields.status: live"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.status: live"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields.status: live"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields.status: live"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields.status: live"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug fields] ***********************
Friday 03 May 2019  13:18:16 -0400 (0:00:00.408)       0:01:16.368 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields: {u'status': u'live', u'Comment': u'', u'customer': u'BCNE', u'ssh_keys': u'1', u'env': u'performance', u'Keys': u'1', u'ip': u'172.29.15.190', u'rsync': u'1', u'server': u'bcne-wl-pf01', u'port': u'22', u'MelissaPush': u'1', u'remote_ssh': u'1', u'user': u'weblogic', u'version': u'56', u'MelissaUser': u'1', u'New': u'1', u'Type': u'hrMultiServerPrimary', u'SLA': u'vranp', u'ARGUMENTS': u'generic,hrMultiServerPrimary'}"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline_fields: {u'status': u'live', u'Comment': u'', u'customer': u'BCNE', u'ssh_keys': u'1', u'env': u'performance', u'Keys': u'1', u'ip': u'172.29.15.192', u'rsync': u'1', u'server': u'bcne-wl-pf03', u'port': u'22', u'MelissaPush': u'1', u'remote_ssh': u'1', u'user': u'weblogic', u'version': u'56', u'MelissaUser': u'1', u'New': u'1', u'Type': u'hrMultiServerSecondary', u'SLA': u'vranp', u'ARGUMENTS': u'generic,hrMultiServerSecondary'}"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields: {u'status': u'live', u'Comment': u'', u'customer': u'BCNE', u'ssh_keys': u'1', u'env': u'performance', u'Keys': u'1', u'ip': u'172.29.15.191', u'rsync': u'1', u'server': u'bcne-wl-pf02', u'port': u'22', u'MelissaPush': u'1', u'remote_ssh': u'1', u'user': u'weblogic', u'version': u'56', u'MelissaUser': u'1', u'New': u'1', u'Type': u'hrMultiServerSecondary', u'SLA': u'vranp', u'ARGUMENTS': u'generic,hrMultiServerSecondary'}"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline_fields: {u'status': u'live', u'Comment': u'', u'customer': u'BCNE', u'ssh_keys': u'1', u'env': u'performance', u'Keys': u'1', u'ip': u'172.29.15.193', u'rsync': u'1', u'server': u'bcne-wl-pf04', u'port': u'22', u'MelissaPush': u'1', u'remote_ssh': u'1', u'user': u'weblogic', u'version': u'56', u'MelissaUser': u'1', u'New': u'1', u'Type': u'hrMultiServerSecondary', u'SLA': u'vranp', u'ARGUMENTS': u'generic,hrMultiServerSecondary'}"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline_fields: {u'status': u'live', u'Comment': u'', u'customer': u'BCNE', u'ssh_keys': u'1', u'env': u'performance', u'Keys': u'1', u'ip': u'172.29.15.195', u'rsync': u'1', u'server': u'bcne-wl-pf06', u'port': u'22', u'MelissaPush': u'1', u'remote_ssh': u'1', u'user': u'weblogic', u'version': u'56', u'MelissaUser': u'1', u'New': u'1', u'Type': u'hrMultiServerSecondary', u'SLA': u'vranp', u'ARGUMENTS': u'generic,hrMultiServerSecondary'}"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline_fields: {u'status': u'live', u'Comment': u'', u'customer': u'BCNE', u'ssh_keys': u'1', u'env': u'performance', u'Keys': u'1', u'ip': u'172.29.15.194', u'rsync': u'1', u'server': u'bcne-wl-pf05', u'port': u'22', u'MelissaPush': u'1', u'remote_ssh': u'1', u'user': u'weblogic', u'version': u'56', u'MelissaUser': u'1', u'New': u'1', u'Type': u'hrMultiServerSecondary', u'SLA': u'vranp', u'ARGUMENTS': u'generic,hrMultiServerSecondary'}"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields: {u'status': u'live', u'Comment': u'', u'customer': u'BCNE', u'ssh_keys': u'1', u'env': u'performance', u'Keys': u'1', u'ip': u'172.29.15.196', u'rsync': u'1', u'server': u'bcne-con-pf01', u'port': u'22', u'MelissaPush': u'0', u'remote_ssh': u'1', u'user': u'cprime', u'version': u'0', u'MelissaUser': u'0', u'New': u'1', u'Type': u'cprime', u'SLA': u'vranp', u'ARGUMENTS': u'generic,cprime'}"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline_fields: {u'status': u'live', u'Comment': u'', u'customer': u'BCNE', u'ssh_keys': u'1', u'env': u'performance', u'Keys': u'1', u'ip': u'172.29.15.198', u'rsync': u'1', u'server': u'bcne-lb-pf01', u'port': u'22', u'MelissaPush': u'0', u'remote_ssh': u'1', u'user': u'httpd', u'version': u'0', u'MelissaUser': u'0', u'New': u'1', u'Type': u'apacheLb', u'SLA': u'vranp', u'ARGUMENTS': u'generic,apacheLb'}"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline_fields: {u'status': u'live', u'Comment': u'', u'customer': u'BCNE', u'ssh_keys': u'1', u'env': u'performance', u'Keys': u'1', u'ip': u'172.29.15.197', u'rsync': u'1', u'server': u'bcne-con-pf02', u'port': u'22', u'MelissaPush': u'0', u'remote_ssh': u'1', u'user': u'cprime', u'version': u'0', u'MelissaUser': u'0', u'New': u'1', u'Type': u'cprime', u'SLA': u'vranp', u'ARGUMENTS': u'generic,cprime'}"
}

TASK [he-debug-ops-utils-cmdb-serverline : debug line] *************************
Friday 03 May 2019  13:18:16 -0400 (0:00:00.320)       0:01:16.689 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "ops_utils_cmdb_serverline:  bcne-wl-pf01\t172.29.15.190\t22\tweblogic\tBCNE\tperformance\thrMultiServerPrimary\t56\tgeneric,hrMultiServerPrimary\t1\t1\t1\t1\t\t1\t1\t1\tvranp\tlive"
}
ok: [bcne-wl-pf02] => {
    "msg": "ops_utils_cmdb_serverline:  bcne-wl-pf02\t172.29.15.191\t22\tweblogic\tBCNE\tperformance\thrMultiServerSecondary\t56\tgeneric,hrMultiServerSecondary\t1\t1\t1\t1\t\t1\t1\t1\tvranp\tlive"
}
ok: [bcne-wl-pf03] => {
    "msg": "ops_utils_cmdb_serverline:  bcne-wl-pf03\t172.29.15.192\t22\tweblogic\tBCNE\tperformance\thrMultiServerSecondary\t56\tgeneric,hrMultiServerSecondary\t1\t1\t1\t1\t\t1\t1\t1\tvranp\tlive"
}
ok: [bcne-wl-pf04] => {
    "msg": "ops_utils_cmdb_serverline:  bcne-wl-pf04\t172.29.15.193\t22\tweblogic\tBCNE\tperformance\thrMultiServerSecondary\t56\tgeneric,hrMultiServerSecondary\t1\t1\t1\t1\t\t1\t1\t1\tvranp\tlive"
}
ok: [bcne-wl-pf05] => {
    "msg": "ops_utils_cmdb_serverline:  bcne-wl-pf05\t172.29.15.194\t22\tweblogic\tBCNE\tperformance\thrMultiServerSecondary\t56\tgeneric,hrMultiServerSecondary\t1\t1\t1\t1\t\t1\t1\t1\tvranp\tlive"
}
ok: [bcne-wl-pf06] => {
    "msg": "ops_utils_cmdb_serverline:  bcne-wl-pf06\t172.29.15.195\t22\tweblogic\tBCNE\tperformance\thrMultiServerSecondary\t56\tgeneric,hrMultiServerSecondary\t1\t1\t1\t1\t\t1\t1\t1\tvranp\tlive"
}
ok: [bcne-con-pf01] => {
    "msg": "ops_utils_cmdb_serverline:  bcne-con-pf01\t172.29.15.196\t22\tcprime\tBCNE\tperformance\tcprime\t0\tgeneric,cprime\t1\t1\t1\t1\t\t1\t0\t0\tvranp\tlive"
}
ok: [bcne-con-pf02] => {
    "msg": "ops_utils_cmdb_serverline:  bcne-con-pf02\t172.29.15.197\t22\tcprime\tBCNE\tperformance\tcprime\t0\tgeneric,cprime\t1\t1\t1\t1\t\t1\t0\t0\tvranp\tlive"
}
ok: [bcne-lb-pf01] => {
    "msg": "ops_utils_cmdb_serverline:  bcne-lb-pf01\t172.29.15.198\t22\thttpd\tBCNE\tperformance\tapacheLb\t0\tgeneric,apacheLb\t1\t1\t1\t1\t\t1\t0\t0\tvranp\tlive"
}

PLAY [Update metadata and configuration files on the control server] ***********

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:18:17 -0400 (0:00:00.481)       0:01:17.170 ************ 
ok: [localhost]

TASK [he-load-metadata : Do we have a override file for this environment?] *****
Friday 03 May 2019  13:18:23 -0400 (0:00:06.647)       0:01:23.817 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Test environment override file] ***********************
Friday 03 May 2019  13:18:24 -0400 (0:00:00.722)       0:01:24.540 ************ 
skipping: [localhost]

TASK [he-load-metadata : Include_vars environment overrides file] **************
Friday 03 May 2019  13:18:24 -0400 (0:00:00.032)       0:01:24.573 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Do we have a default customer.yml for this customer?] ***
Friday 03 May 2019  13:18:24 -0400 (0:00:00.044)       0:01:24.617 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Test customer.yml] ************************************
Friday 03 May 2019  13:18:25 -0400 (0:00:00.725)       0:01:25.342 ************ 
skipping: [localhost]

TASK [he-load-metadata : Include_vars customer.yml] ****************************
Friday 03 May 2019  13:18:25 -0400 (0:00:00.032)       0:01:25.375 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Do we have an SLA-specific customer-<SLA>.yml for this customer?] ***
Friday 03 May 2019  13:18:25 -0400 (0:00:00.044)       0:01:25.420 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include SLA-Specific customer.yml] ********************
Friday 03 May 2019  13:18:26 -0400 (0:00:00.703)       0:01:26.123 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Do we have a diff-by-sla file for this SLA?] **********
Friday 03 May 2019  13:18:26 -0400 (0:00:00.052)       0:01:26.175 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include default diff-by-sla meta-data] ****************
Friday 03 May 2019  13:18:26 -0400 (0:00:00.268)       0:01:26.444 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include SLA-specific diff-by-sla meta-data] ***********
Friday 03 May 2019  13:18:26 -0400 (0:00:00.056)       0:01:26.500 ************ 
skipping: [localhost]

TASK [he-load-metadata : Do we have a defaults.yml in the metadata locale ?] ***
Friday 03 May 2019  13:18:26 -0400 (0:00:00.031)       0:01:26.532 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include metadata defaults.yml] ************************
Friday 03 May 2019  13:18:27 -0400 (0:00:00.698)       0:01:27.231 ************ 
 [WARNING]: While constructing a mapping from True, line 4, column 1, found a
duplicate dict key (caremanager_template_name). Using last defined value only.
ok: [localhost -> localhost]

TASK [he-load-metadata : Include_vars environment overrides file to key into provisioner region] ***
Friday 03 May 2019  13:18:27 -0400 (0:00:00.073)       0:01:27.304 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include Provisioner-region metadata] ******************
Friday 03 May 2019  13:18:27 -0400 (0:00:00.047)       0:01:27.352 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include_vars environment overrides file to key into provisioner region] ***
Friday 03 May 2019  13:18:27 -0400 (0:00:00.046)       0:01:27.399 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include base secrets file] ****************************
Friday 03 May 2019  13:18:27 -0400 (0:00:00.043)       0:01:27.442 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include provisioner secrets file] *********************
Friday 03 May 2019  13:18:27 -0400 (0:00:00.052)       0:01:27.495 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Do we have region specific secrets for this provisioner ?] ***
Friday 03 May 2019  13:18:27 -0400 (0:00:00.040)       0:01:27.536 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include region specific secrets file] *****************
Friday 03 May 2019  13:18:27 -0400 (0:00:00.237)       0:01:27.773 ************ 
skipping: [localhost]

TASK [he-load-metadata : Do we have generic customer secrets file?] ************
Friday 03 May 2019  13:18:27 -0400 (0:00:00.065)       0:01:27.838 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include generic customer secrets file] ****************
Friday 03 May 2019  13:18:28 -0400 (0:00:00.689)       0:01:28.527 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Do we have customer-environment specific secrets ?] ***
Friday 03 May 2019  13:18:28 -0400 (0:00:00.046)       0:01:28.574 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include customer-environment specific secrets] ********
Friday 03 May 2019  13:18:29 -0400 (0:00:00.740)       0:01:29.314 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include_vars environment overrides file one last time to catch overrides] ***
Friday 03 May 2019  13:18:29 -0400 (0:00:00.050)       0:01:29.365 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Do we have a default customer.yml for this customer?] ***
Friday 03 May 2019  13:18:29 -0400 (0:00:00.042)       0:01:29.407 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Test customer.yml] ************************************
Friday 03 May 2019  13:18:30 -0400 (0:00:00.682)       0:01:30.090 ************ 
skipping: [localhost]

TASK [he-load-metadata : Include_vars customer.yml] ****************************
Friday 03 May 2019  13:18:30 -0400 (0:00:00.033)       0:01:30.124 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Do we have an SLA-specific customer-<SLA>.yml for this customer?] ***
Friday 03 May 2019  13:18:30 -0400 (0:00:00.048)       0:01:30.172 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include SLA-Specific customer.yml] ********************
Friday 03 May 2019  13:18:30 -0400 (0:00:00.723)       0:01:30.896 ************ 
ok: [localhost -> localhost]

TASK [he-load-metadata : Include_vars environment overrides file one last time to catch overrides] ***
Friday 03 May 2019  13:18:30 -0400 (0:00:00.048)       0:01:30.944 ************ 
ok: [localhost -> localhost]

TASK [he-add-serverlist-cmdb : Sync the environments repo using git] ***********
Friday 03 May 2019  13:18:30 -0400 (0:00:00.044)       0:01:30.989 ************ 
ok: [localhost -> localhost]

TASK [he-add-serverlist-cmdb : Add host to global servers file] ****************
Friday 03 May 2019  13:18:32 -0400 (0:00:01.214)       0:01:32.203 ************ 
ok: [localhost] => (item=bcne-wl-pf01)
ok: [localhost] => (item=bcne-wl-pf02)
ok: [localhost] => (item=bcne-wl-pf03)
ok: [localhost] => (item=bcne-wl-pf04)
ok: [localhost] => (item=bcne-wl-pf05)
ok: [localhost] => (item=bcne-wl-pf06)
ok: [localhost] => (item=bcne-con-pf01)
ok: [localhost] => (item=bcne-con-pf02)
ok: [localhost] => (item=bcne-lb-pf01)

TASK [he-add-serverlist-cmdb : debug] ******************************************
Friday 03 May 2019  13:18:35 -0400 (0:00:02.904)       0:01:35.107 ************ 
ok: [localhost] => {
    "hosts_changed.changed": false
}

TASK [he-add-serverlist-cmdb : Commit servers file using git] ******************
Friday 03 May 2019  13:18:35 -0400 (0:00:00.041)       0:01:35.149 ************ 
skipping: [localhost]

TASK [he-generate-installer-properties : Remove non-product specific generated properties directory. Use product specific directories.] ***
Friday 03 May 2019  13:18:35 -0400 (0:00:00.032)       0:01:35.181 ************ 
ok: [localhost]

TASK [he-generate-installer-properties : Create generated properties files directories] ***
Friday 03 May 2019  13:18:35 -0400 (0:00:00.260)       0:01:35.442 ************ 
changed: [localhost] => (item=.)
changed: [localhost] => (item=care-admin)
changed: [localhost] => (item=connector)
changed: [localhost] => (item=custom)
changed: [localhost] => (item=hosts)
changed: [localhost] => (item=payor)

TASK [he-generate-installer-properties : Generate pushbutton.yml] **************
Friday 03 May 2019  13:18:36 -0400 (0:00:01.358)       0:01:36.800 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate hosts/payor_hosts] ***********
Friday 03 May 2019  13:18:37 -0400 (0:00:00.712)       0:01:37.513 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate hosts/connector_hosts] *******
Friday 03 May 2019  13:18:38 -0400 (0:00:00.999)       0:01:38.513 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate payor/custom-files.txt] ******
Friday 03 May 2019  13:18:39 -0400 (0:00:00.868)       0:01:39.381 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate payor/payor.yml] *************
Friday 03 May 2019  13:18:39 -0400 (0:00:00.563)       0:01:39.945 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate payor/wl.properties] *********
Friday 03 May 2019  13:18:40 -0400 (0:00:00.496)       0:01:40.441 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate care-admin/PayorCareAdminConfiguration.xml] ***
Friday 03 May 2019  13:18:41 -0400 (0:00:01.031)       0:01:41.473 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate care-admin/server.properties] ***
Friday 03 May 2019  13:18:41 -0400 (0:00:00.525)       0:01:41.998 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate connector/connector.yml] *****
Friday 03 May 2019  13:18:42 -0400 (0:00:00.527)       0:01:42.525 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate connector/connector_fresh_install.properties] ***
Friday 03 May 2019  13:18:43 -0400 (0:00:00.556)       0:01:43.081 ************ 
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/templates/connector/connector_fresh_install_18.2.properties.j2)

TASK [he-generate-installer-properties : Generate connector/connector_upgrade_new_configs.properties] ***
Friday 03 May 2019  13:18:43 -0400 (0:00:00.680)       0:01:43.762 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate connector/custom_fresh_install.properties] ***
Friday 03 May 2019  13:18:44 -0400 (0:00:00.493)       0:01:44.255 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate custom/custom.properties] ****
Friday 03 May 2019  13:18:44 -0400 (0:00:00.504)       0:01:44.760 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate custom/custom.yml] ***********
Friday 03 May 2019  13:18:45 -0400 (0:00:00.542)       0:01:45.302 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Insure connector distribution is present] ***
Friday 03 May 2019  13:18:45 -0400 (0:00:00.583)       0:01:45.886 ************ 
ok: [localhost -> localhost]

TASK [he-generate-installer-properties : fail] *********************************
Friday 03 May 2019  13:18:50 -0400 (0:00:04.295)       0:01:50.182 ************ 
skipping: [localhost]

TASK [he-generate-installer-properties : Manifest-search: get file-list from connector distrib.] ***
Friday 03 May 2019  13:18:50 -0400 (0:00:00.033)       0:01:50.216 ************ 
changed: [localhost -> localhost]

TASK [he-generate-installer-properties : debug] ********************************
Friday 03 May 2019  13:18:55 -0400 (0:00:05.648)       0:01:55.865 ************ 
skipping: [localhost]

TASK [he-generate-installer-properties : debug] ********************************
Friday 03 May 2019  13:18:55 -0400 (0:00:00.029)       0:01:55.894 ************ 
ok: [localhost] => {
    "msg": "!!!NOMANIFEST!!!"
}

TASK [he-generate-installer-properties : Connector host.properties set_fact] ***
Friday 03 May 2019  13:18:55 -0400 (0:00:00.039)       0:01:55.934 ************ 
ok: [localhost]

TASK [he-generate-installer-properties : Generate connector/<host>.properties] ***
Friday 03 May 2019  13:18:55 -0400 (0:00:00.043)       0:01:55.977 ************ 
changed: [localhost] => (item=bcne-con-pf01)
changed: [localhost] => (item=bcne-con-pf02)

TASK [he-generate-installer-properties : Create tmp directory] *****************
Friday 03 May 2019  13:18:57 -0400 (0:00:01.282)       0:01:57.260 ************ 
skipping: [localhost]

TASK [he-generate-installer-properties : Extract the manifest from the connector distrib.] ***
Friday 03 May 2019  13:18:57 -0400 (0:00:00.065)       0:01:57.326 ************ 
skipping: [localhost]

TASK [he-generate-installer-properties : Get connector distrib manifest contents] ***
Friday 03 May 2019  13:18:57 -0400 (0:00:00.036)       0:01:57.363 ************ 
skipping: [localhost]

TASK [he-generate-installer-properties : Generate connector/<host>.properties] ***
Friday 03 May 2019  13:18:57 -0400 (0:00:00.032)       0:01:57.395 ************ 
skipping: [localhost] => (item=bcne-con-pf01) 
skipping: [localhost] => (item=bcne-con-pf02) 

TASK [he-generate-installer-properties : Clean up tmp directory] ***************
Friday 03 May 2019  13:18:57 -0400 (0:00:00.058)       0:01:57.454 ************ 
skipping: [localhost]

TASK [he-generate-installer-properties : debug] ********************************
Friday 03 May 2019  13:18:57 -0400 (0:00:00.030)       0:01:57.484 ************ 
ok: [localhost] => (item=bcne-con-pf01) => {
    "changed": false, 
    "item": "bcne-con-pf01", 
    "msg": "172.29.15.196"
}
ok: [localhost] => (item=bcne-con-pf02) => {
    "changed": false, 
    "item": "bcne-con-pf02", 
    "msg": "172.29.15.197"
}

TASK [he-generate-installer-properties : Generate connector/<host>-connector-features-configs.properties] ***
Friday 03 May 2019  13:18:57 -0400 (0:00:00.192)       0:01:57.676 ************ 
changed: [localhost -> localhost] => (item=bcne-con-pf01)
changed: [localhost -> localhost] => (item=bcne-con-pf02)

TASK [he-generate-installer-properties : Generate connector/<host>_custom.properties] ***
Friday 03 May 2019  13:18:58 -0400 (0:00:00.650)       0:01:58.327 ************ 
changed: [localhost -> localhost] => (item=bcne-con-pf01)
changed: [localhost -> localhost] => (item=bcne-con-pf02)

TASK [he-generate-installer-properties : Generate connector/<host>-connector-custom-features-configs.properties] ***
Friday 03 May 2019  13:18:58 -0400 (0:00:00.632)       0:01:58.959 ************ 
changed: [localhost -> localhost] => (item=bcne-con-pf01)
changed: [localhost -> localhost] => (item=bcne-con-pf02)

TASK [he-generate-installer-properties : Create generated properties files directories] ***
Friday 03 May 2019  13:18:59 -0400 (0:00:00.628)       0:01:59.587 ************ 
changed: [localhost] => (item=config/group_vars/all)
changed: [localhost] => (item=custom_files)
changed: [localhost] => (item=config/group_vars/windows-servers)

TASK [he-generate-installer-properties : Generate cm group_vars/all files] *****
Friday 03 May 2019  13:19:00 -0400 (0:00:00.714)       0:02:00.302 ************ 
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager/answers.yml.j2)
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager/caremanager_locations.yml.j2)
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager/carepathways.yml.j2)
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager/he-installer.environment.yml.j2)
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager/tasktool.yml.j2)
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager/caremanager.yml.j2)
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager/caremanager_careadmin.yml.j2)
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager/caremanager_system_software.yml.j2)
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager/integration-services.yml.j2)
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager/providersearch.yml.j2)

TASK [he-generate-installer-properties : Generate group_vars/windows-servers files] ***
Friday 03 May 2019  13:19:05 -0400 (0:00:05.063)       0:02:05.366 ************ 
changed: [localhost] => (item=/home/ansible/hebb/installer/common/roles/he-generate-installer-properties/tasks/../templates/caremanager-windows-servers/windows-servers.yml.j2)

TASK [he-generate-installer-properties : Generate CM hosts file] ***************
Friday 03 May 2019  13:19:05 -0400 (0:00:00.530)       0:02:05.896 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Copy ipg_pch_data file] ***************
Friday 03 May 2019  13:19:06 -0400 (0:00:00.499)       0:02:06.396 ************ 
skipping: [localhost]

TASK [he-generate-installer-properties : Copy c4c_ca_ext_sql_file file] ********
Friday 03 May 2019  13:19:06 -0400 (0:00:00.030)       0:02:06.427 ************ 
skipping: [localhost]

TASK [he-generate-installer-properties : Create generated answers properties directory] ***
Friday 03 May 2019  13:19:06 -0400 (0:00:00.028)       0:02:06.455 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Generate answers.yml] *****************
Friday 03 May 2019  13:19:06 -0400 (0:00:00.235)       0:02:06.690 ************ 
changed: [localhost]

TASK [he-generate-installer-properties : Create Install script] ****************
Friday 03 May 2019  13:19:07 -0400 (0:00:00.599)       0:02:07.289 ************ 
changed: [localhost]

PLAY [Add cmdb metadata] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:07 -0400 (0:00:00.515)       0:02:07.804 ************ 
ok: [bcne-wl-pf01]

TASK [he-add-envproperties-cmdb : figure out if autobatch is enabled] **********
Friday 03 May 2019  13:19:08 -0400 (0:00:00.479)       0:02:08.284 ************ 
ok: [bcne-wl-pf01]

TASK [he-add-envproperties-cmdb : See if properties ( ../../../environments/config/CMDB/envProperties/userServer/env.userServer.weblogic.bcne-wl-pf01 )file is present] ***
Friday 03 May 2019  13:19:08 -0400 (0:00:00.066)       0:02:08.350 ************ 
ok: [bcne-wl-pf01 -> localhost]

TASK [he-add-envproperties-cmdb : Sync the environments repo using git] ********
Friday 03 May 2019  13:19:08 -0400 (0:00:00.236)       0:02:08.586 ************ 
ok: [bcne-wl-pf01 -> localhost]

TASK [he-add-envproperties-cmdb : Ensure envProperties is present] *************
Friday 03 May 2019  13:19:09 -0400 (0:00:01.048)       0:02:09.634 ************ 
skipping: [bcne-wl-pf01]

TASK [he-add-envproperties-cmdb : Add autobatch cmdline to envProperties unless it is empty] ***
Friday 03 May 2019  13:19:09 -0400 (0:00:00.039)       0:02:09.674 ************ 
ok: [bcne-wl-pf01 -> localhost]

TASK [he-add-envproperties-cmdb : Add autobatch enable block to envProperties] ***
Friday 03 May 2019  13:19:09 -0400 (0:00:00.288)       0:02:09.962 ************ 
ok: [bcne-wl-pf01 -> localhost]

TASK [he-add-envproperties-cmdb : Commit userServers envProperties file using git] ***
Friday 03 May 2019  13:19:10 -0400 (0:00:00.474)       0:02:10.437 ************ 
skipping: [bcne-wl-pf01]

PLAY [Add cmdb metadata] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:10 -0400 (0:00:00.039)       0:02:10.476 ************ 
ok: [bcne-wl-pf03]

TASK [he-add-envproperties-cmdb : figure out if autobatch is enabled] **********
Friday 03 May 2019  13:19:10 -0400 (0:00:00.465)       0:02:10.942 ************ 
skipping: [bcne-wl-pf03]

TASK [he-add-envproperties-cmdb : See if properties ( ../../../environments/config/CMDB/envProperties/userServer/env.userServer.weblogic.bcne-wl-pf03 )file is present] ***
Friday 03 May 2019  13:19:10 -0400 (0:00:00.033)       0:02:10.976 ************ 
ok: [bcne-wl-pf03 -> localhost]

TASK [he-add-envproperties-cmdb : Sync the environments repo using git] ********
Friday 03 May 2019  13:19:11 -0400 (0:00:00.237)       0:02:11.214 ************ 
ok: [bcne-wl-pf03 -> localhost]

TASK [he-add-envproperties-cmdb : Ensure envProperties is present] *************
Friday 03 May 2019  13:19:12 -0400 (0:00:01.097)       0:02:12.312 ************ 
skipping: [bcne-wl-pf03]

TASK [he-add-envproperties-cmdb : Add autobatch cmdline to envProperties unless it is empty] ***
Friday 03 May 2019  13:19:12 -0400 (0:00:00.031)       0:02:12.343 ************ 
skipping: [bcne-wl-pf03]

TASK [he-add-envproperties-cmdb : Add autobatch enable block to envProperties] ***
Friday 03 May 2019  13:19:12 -0400 (0:00:00.033)       0:02:12.376 ************ 
skipping: [bcne-wl-pf03]

TASK [he-add-envproperties-cmdb : Commit userServers envProperties file using git] ***
Friday 03 May 2019  13:19:12 -0400 (0:00:00.030)       0:02:12.406 ************ 
skipping: [bcne-wl-pf03]

PLAY [Add cmdb metadata] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:12 -0400 (0:00:00.032)       0:02:12.439 ************ 
ok: [bcne-wl-pf02]

TASK [he-add-envproperties-cmdb : figure out if autobatch is enabled] **********
Friday 03 May 2019  13:19:12 -0400 (0:00:00.469)       0:02:12.909 ************ 
skipping: [bcne-wl-pf02]

TASK [he-add-envproperties-cmdb : See if properties ( ../../../environments/config/CMDB/envProperties/userServer/env.userServer.weblogic.bcne-wl-pf02 )file is present] ***
Friday 03 May 2019  13:19:12 -0400 (0:00:00.041)       0:02:12.950 ************ 
ok: [bcne-wl-pf02 -> localhost]

TASK [he-add-envproperties-cmdb : Sync the environments repo using git] ********
Friday 03 May 2019  13:19:13 -0400 (0:00:00.245)       0:02:13.195 ************ 
ok: [bcne-wl-pf02 -> localhost]

TASK [he-add-envproperties-cmdb : Ensure envProperties is present] *************
Friday 03 May 2019  13:19:14 -0400 (0:00:01.044)       0:02:14.239 ************ 
skipping: [bcne-wl-pf02]

TASK [he-add-envproperties-cmdb : Add autobatch cmdline to envProperties unless it is empty] ***
Friday 03 May 2019  13:19:14 -0400 (0:00:00.046)       0:02:14.286 ************ 
skipping: [bcne-wl-pf02]

TASK [he-add-envproperties-cmdb : Add autobatch enable block to envProperties] ***
Friday 03 May 2019  13:19:14 -0400 (0:00:00.041)       0:02:14.327 ************ 
skipping: [bcne-wl-pf02]

TASK [he-add-envproperties-cmdb : Commit userServers envProperties file using git] ***
Friday 03 May 2019  13:19:14 -0400 (0:00:00.031)       0:02:14.359 ************ 
skipping: [bcne-wl-pf02]

PLAY [Add cmdb metadata] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:14 -0400 (0:00:00.037)       0:02:14.396 ************ 
ok: [bcne-wl-pf04]

TASK [he-add-envproperties-cmdb : figure out if autobatch is enabled] **********
Friday 03 May 2019  13:19:14 -0400 (0:00:00.467)       0:02:14.864 ************ 
skipping: [bcne-wl-pf04]

TASK [he-add-envproperties-cmdb : See if properties ( ../../../environments/config/CMDB/envProperties/userServer/env.userServer.weblogic.bcne-wl-pf04 )file is present] ***
Friday 03 May 2019  13:19:14 -0400 (0:00:00.034)       0:02:14.898 ************ 
ok: [bcne-wl-pf04 -> localhost]

TASK [he-add-envproperties-cmdb : Sync the environments repo using git] ********
Friday 03 May 2019  13:19:15 -0400 (0:00:00.232)       0:02:15.130 ************ 
ok: [bcne-wl-pf04 -> localhost]

TASK [he-add-envproperties-cmdb : Ensure envProperties is present] *************
Friday 03 May 2019  13:19:16 -0400 (0:00:00.983)       0:02:16.114 ************ 
skipping: [bcne-wl-pf04]

TASK [he-add-envproperties-cmdb : Add autobatch cmdline to envProperties unless it is empty] ***
Friday 03 May 2019  13:19:16 -0400 (0:00:00.032)       0:02:16.146 ************ 
skipping: [bcne-wl-pf04]

TASK [he-add-envproperties-cmdb : Add autobatch enable block to envProperties] ***
Friday 03 May 2019  13:19:16 -0400 (0:00:00.040)       0:02:16.186 ************ 
skipping: [bcne-wl-pf04]

TASK [he-add-envproperties-cmdb : Commit userServers envProperties file using git] ***
Friday 03 May 2019  13:19:16 -0400 (0:00:00.029)       0:02:16.216 ************ 
skipping: [bcne-wl-pf04]

PLAY [Add cmdb metadata] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:16 -0400 (0:00:00.040)       0:02:16.257 ************ 
ok: [bcne-wl-pf05]

TASK [he-add-envproperties-cmdb : figure out if autobatch is enabled] **********
Friday 03 May 2019  13:19:16 -0400 (0:00:00.481)       0:02:16.738 ************ 
skipping: [bcne-wl-pf05]

TASK [he-add-envproperties-cmdb : See if properties ( ../../../environments/config/CMDB/envProperties/userServer/env.userServer.weblogic.bcne-wl-pf05 )file is present] ***
Friday 03 May 2019  13:19:16 -0400 (0:00:00.041)       0:02:16.779 ************ 
ok: [bcne-wl-pf05 -> localhost]

TASK [he-add-envproperties-cmdb : Sync the environments repo using git] ********
Friday 03 May 2019  13:19:17 -0400 (0:00:00.252)       0:02:17.032 ************ 
ok: [bcne-wl-pf05 -> localhost]

TASK [he-add-envproperties-cmdb : Ensure envProperties is present] *************
Friday 03 May 2019  13:19:18 -0400 (0:00:01.016)       0:02:18.049 ************ 
skipping: [bcne-wl-pf05]

TASK [he-add-envproperties-cmdb : Add autobatch cmdline to envProperties unless it is empty] ***
Friday 03 May 2019  13:19:18 -0400 (0:00:00.032)       0:02:18.082 ************ 
skipping: [bcne-wl-pf05]

TASK [he-add-envproperties-cmdb : Add autobatch enable block to envProperties] ***
Friday 03 May 2019  13:19:18 -0400 (0:00:00.030)       0:02:18.112 ************ 
skipping: [bcne-wl-pf05]

TASK [he-add-envproperties-cmdb : Commit userServers envProperties file using git] ***
Friday 03 May 2019  13:19:18 -0400 (0:00:00.027)       0:02:18.140 ************ 
skipping: [bcne-wl-pf05]

PLAY [Add cmdb metadata] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:18 -0400 (0:00:00.038)       0:02:18.178 ************ 
ok: [bcne-wl-pf06]

TASK [he-add-envproperties-cmdb : figure out if autobatch is enabled] **********
Friday 03 May 2019  13:19:18 -0400 (0:00:00.490)       0:02:18.668 ************ 
skipping: [bcne-wl-pf06]

TASK [he-add-envproperties-cmdb : See if properties ( ../../../environments/config/CMDB/envProperties/userServer/env.userServer.weblogic.bcne-wl-pf06 )file is present] ***
Friday 03 May 2019  13:19:18 -0400 (0:00:00.036)       0:02:18.705 ************ 
ok: [bcne-wl-pf06 -> localhost]

TASK [he-add-envproperties-cmdb : Sync the environments repo using git] ********
Friday 03 May 2019  13:19:18 -0400 (0:00:00.277)       0:02:18.982 ************ 
ok: [bcne-wl-pf06 -> localhost]

TASK [he-add-envproperties-cmdb : Ensure envProperties is present] *************
Friday 03 May 2019  13:19:19 -0400 (0:00:00.938)       0:02:19.921 ************ 
skipping: [bcne-wl-pf06]

TASK [he-add-envproperties-cmdb : Add autobatch cmdline to envProperties unless it is empty] ***
Friday 03 May 2019  13:19:19 -0400 (0:00:00.031)       0:02:19.953 ************ 
skipping: [bcne-wl-pf06]

TASK [he-add-envproperties-cmdb : Add autobatch enable block to envProperties] ***
Friday 03 May 2019  13:19:19 -0400 (0:00:00.031)       0:02:19.984 ************ 
skipping: [bcne-wl-pf06]

TASK [he-add-envproperties-cmdb : Commit userServers envProperties file using git] ***
Friday 03 May 2019  13:19:20 -0400 (0:00:00.026)       0:02:20.011 ************ 
skipping: [bcne-wl-pf06]

PLAY [Add cmdb metadata] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:20 -0400 (0:00:00.036)       0:02:20.047 ************ 
ok: [bcne-con-pf01]

TASK [he-add-envproperties-cmdb : figure out if autobatch is enabled] **********
Friday 03 May 2019  13:19:20 -0400 (0:00:00.482)       0:02:20.529 ************ 
skipping: [bcne-con-pf01]

TASK [he-add-envproperties-cmdb : See if properties ( ../../../environments/config/CMDB/envProperties/userServer/env.userServer.cprime.bcne-con-pf01 )file is present] ***
Friday 03 May 2019  13:19:20 -0400 (0:00:00.035)       0:02:20.565 ************ 
ok: [bcne-con-pf01 -> localhost]

TASK [he-add-envproperties-cmdb : Sync the environments repo using git] ********
Friday 03 May 2019  13:19:20 -0400 (0:00:00.271)       0:02:20.836 ************ 
ok: [bcne-con-pf01 -> localhost]

TASK [he-add-envproperties-cmdb : Ensure envProperties is present] *************
Friday 03 May 2019  13:19:21 -0400 (0:00:01.010)       0:02:21.847 ************ 
skipping: [bcne-con-pf01]

TASK [he-add-envproperties-cmdb : Add autobatch cmdline to envProperties unless it is empty] ***
Friday 03 May 2019  13:19:21 -0400 (0:00:00.031)       0:02:21.879 ************ 
skipping: [bcne-con-pf01]

TASK [he-add-envproperties-cmdb : Add autobatch enable block to envProperties] ***
Friday 03 May 2019  13:19:21 -0400 (0:00:00.030)       0:02:21.910 ************ 
skipping: [bcne-con-pf01]

TASK [he-add-envproperties-cmdb : Commit userServers envProperties file using git] ***
Friday 03 May 2019  13:19:21 -0400 (0:00:00.028)       0:02:21.939 ************ 
skipping: [bcne-con-pf01]

PLAY [Add cmdb metadata] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:21 -0400 (0:00:00.035)       0:02:21.974 ************ 
ok: [bcne-con-pf02]

TASK [he-add-envproperties-cmdb : figure out if autobatch is enabled] **********
Friday 03 May 2019  13:19:22 -0400 (0:00:00.464)       0:02:22.438 ************ 
skipping: [bcne-con-pf02]

TASK [he-add-envproperties-cmdb : See if properties ( ../../../environments/config/CMDB/envProperties/userServer/env.userServer.cprime.bcne-con-pf02 )file is present] ***
Friday 03 May 2019  13:19:22 -0400 (0:00:00.033)       0:02:22.472 ************ 
ok: [bcne-con-pf02 -> localhost]

TASK [he-add-envproperties-cmdb : Sync the environments repo using git] ********
Friday 03 May 2019  13:19:22 -0400 (0:00:00.229)       0:02:22.701 ************ 
ok: [bcne-con-pf02 -> localhost]

TASK [he-add-envproperties-cmdb : Ensure envProperties is present] *************
Friday 03 May 2019  13:19:23 -0400 (0:00:00.982)       0:02:23.684 ************ 
skipping: [bcne-con-pf02]

TASK [he-add-envproperties-cmdb : Add autobatch cmdline to envProperties unless it is empty] ***
Friday 03 May 2019  13:19:23 -0400 (0:00:00.043)       0:02:23.727 ************ 
skipping: [bcne-con-pf02]

TASK [he-add-envproperties-cmdb : Add autobatch enable block to envProperties] ***
Friday 03 May 2019  13:19:23 -0400 (0:00:00.045)       0:02:23.772 ************ 
skipping: [bcne-con-pf02]

TASK [he-add-envproperties-cmdb : Commit userServers envProperties file using git] ***
Friday 03 May 2019  13:19:23 -0400 (0:00:00.062)       0:02:23.835 ************ 
skipping: [bcne-con-pf02]

PLAY [Add cmdb metadata] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:23 -0400 (0:00:00.036)       0:02:23.872 ************ 
ok: [bcne-lb-pf01]

TASK [he-add-envproperties-cmdb : figure out if autobatch is enabled] **********
Friday 03 May 2019  13:19:24 -0400 (0:00:00.544)       0:02:24.417 ************ 
skipping: [bcne-lb-pf01]

TASK [he-add-envproperties-cmdb : See if properties ( ../../../environments/config/CMDB/envProperties/userServer/env.userServer.httpd.bcne-lb-pf01 )file is present] ***
Friday 03 May 2019  13:19:24 -0400 (0:00:00.034)       0:02:24.451 ************ 
ok: [bcne-lb-pf01 -> localhost]

TASK [he-add-envproperties-cmdb : Sync the environments repo using git] ********
Friday 03 May 2019  13:19:24 -0400 (0:00:00.236)       0:02:24.688 ************ 
ok: [bcne-lb-pf01 -> localhost]

TASK [he-add-envproperties-cmdb : Ensure envProperties is present] *************
Friday 03 May 2019  13:19:25 -0400 (0:00:00.997)       0:02:25.685 ************ 
skipping: [bcne-lb-pf01]

TASK [he-add-envproperties-cmdb : Add autobatch cmdline to envProperties unless it is empty] ***
Friday 03 May 2019  13:19:25 -0400 (0:00:00.042)       0:02:25.728 ************ 
skipping: [bcne-lb-pf01]

TASK [he-add-envproperties-cmdb : Add autobatch enable block to envProperties] ***
Friday 03 May 2019  13:19:25 -0400 (0:00:00.039)       0:02:25.767 ************ 
skipping: [bcne-lb-pf01]

TASK [he-add-envproperties-cmdb : Commit userServers envProperties file using git] ***
Friday 03 May 2019  13:19:25 -0400 (0:00:00.039)       0:02:25.807 ************ 
skipping: [bcne-lb-pf01]

PLAY [Serial Yum update] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:25 -0400 (0:00:00.053)       0:02:25.860 ************ 
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf02]

TASK [he-yum-internal-repo : Create he_backup directory for repos] *************
Friday 03 May 2019  13:19:26 -0400 (0:00:00.714)       0:02:26.575 ************ 
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf03]

TASK [he-yum-internal-repo : Copy key file for epel-release] *******************
Friday 03 May 2019  13:19:27 -0400 (0:00:00.611)       0:02:27.187 ************ 
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf01]

TASK [he-yum-internal-repo : Turn off plugins in yum.conf] *********************
Friday 03 May 2019  13:19:28 -0400 (0:00:00.850)       0:02:28.038 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]

TASK [he-yum-internal-repo : Turn off external internet repo] ******************
Friday 03 May 2019  13:19:28 -0400 (0:00:00.621)       0:02:28.660 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf03]

TASK [he-yum-internal-repo : Configure HealthEdge Internal Nexus repo] *********
Friday 03 May 2019  13:19:29 -0400 (0:00:00.627)       0:02:29.287 ************ 
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]

TASK [he-yum-internal-repo : Configure HealthEdge Internal Nexus EPEL repo] ****
Friday 03 May 2019  13:19:30 -0400 (0:00:00.846)       0:02:30.134 ************ 
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]

TASK [he-yum-internal-repo : Configure HealthEdge IUS repo] ********************
Friday 03 May 2019  13:19:31 -0400 (0:00:00.942)       0:02:31.077 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]

TASK [he-yum-internal-repo : Configure HealthEdge Internal IUS repo] ***********
Friday 03 May 2019  13:19:31 -0400 (0:00:00.075)       0:02:31.152 ************ 
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf01]

TASK [he-yum-internal-repo : Copy key file for IUS repo] ***********************
Friday 03 May 2019  13:19:32 -0400 (0:00:00.957)       0:02:32.110 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]

TASK [he-yum-installs : install yum packages all] ******************************
Friday 03 May 2019  13:19:33 -0400 (0:00:01.110)       0:02:33.220 ************ 
changed: [bcne-wl-pf01] => (item=[u'tree', u'telnet', u'mutt', u'firewalld', u'rsync', u'unzip'])
changed: [bcne-wl-pf03] => (item=[u'tree', u'telnet', u'mutt', u'firewalld', u'rsync', u'unzip'])
changed: [bcne-wl-pf02] => (item=[u'tree', u'telnet', u'mutt', u'firewalld', u'rsync', u'unzip'])

TASK [he-yum-installs : install yum packages group] ****************************
Friday 03 May 2019  13:19:49 -0400 (0:00:15.959)       0:02:49.180 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]

TASK [he-yum-installs : check for Metacity rpm installed] **********************
Friday 03 May 2019  13:19:49 -0400 (0:00:00.080)       0:02:49.260 ************ 
 [WARNING]: Consider using yum, dnf or zypper module rather than running rpm
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf02]

TASK [he-yum-installs : remove Metacity rpm] ***********************************
Friday 03 May 2019  13:19:51 -0400 (0:00:01.907)       0:02:51.168 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]

TASK [he-yum-installs : Update to latest packages] *****************************
Friday 03 May 2019  13:19:52 -0400 (0:00:01.031)       0:02:52.200 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]

PLAY [Serial Yum update] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:19:52 -0400 (0:00:00.129)       0:02:52.330 ************ 
ok: [bcne-wl-pf06]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf05]

TASK [he-yum-internal-repo : Create he_backup directory for repos] *************
Friday 03 May 2019  13:19:53 -0400 (0:00:00.733)       0:02:53.063 ************ 
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf06]

TASK [he-yum-internal-repo : Copy key file for epel-release] *******************
Friday 03 May 2019  13:19:53 -0400 (0:00:00.461)       0:02:53.524 ************ 
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]
ok: [bcne-wl-pf04]

TASK [he-yum-internal-repo : Turn off plugins in yum.conf] *********************
Friday 03 May 2019  13:19:54 -0400 (0:00:00.648)       0:02:54.173 ************ 
changed: [bcne-wl-pf06]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]

TASK [he-yum-internal-repo : Turn off external internet repo] ******************
Friday 03 May 2019  13:19:54 -0400 (0:00:00.447)       0:02:54.620 ************ 
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf06]

TASK [he-yum-internal-repo : Configure HealthEdge Internal Nexus repo] *********
Friday 03 May 2019  13:19:55 -0400 (0:00:00.477)       0:02:55.098 ************ 
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf06]

TASK [he-yum-internal-repo : Configure HealthEdge Internal Nexus EPEL repo] ****
Friday 03 May 2019  13:19:55 -0400 (0:00:00.742)       0:02:55.840 ************ 
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf06]
changed: [bcne-wl-pf05]

TASK [he-yum-internal-repo : Configure HealthEdge IUS repo] ********************
Friday 03 May 2019  13:19:56 -0400 (0:00:00.715)       0:02:56.555 ************ 
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]

TASK [he-yum-internal-repo : Configure HealthEdge Internal IUS repo] ***********
Friday 03 May 2019  13:19:56 -0400 (0:00:00.058)       0:02:56.614 ************ 
changed: [bcne-wl-pf06]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]

TASK [he-yum-internal-repo : Copy key file for IUS repo] ***********************
Friday 03 May 2019  13:19:57 -0400 (0:00:00.767)       0:02:57.382 ************ 
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf06]

TASK [he-yum-installs : install yum packages all] ******************************
Friday 03 May 2019  13:19:58 -0400 (0:00:00.704)       0:02:58.087 ************ 
changed: [bcne-wl-pf05] => (item=[u'tree', u'telnet', u'mutt', u'firewalld', u'rsync', u'unzip'])
changed: [bcne-wl-pf04] => (item=[u'tree', u'telnet', u'mutt', u'firewalld', u'rsync', u'unzip'])
changed: [bcne-wl-pf06] => (item=[u'tree', u'telnet', u'mutt', u'firewalld', u'rsync', u'unzip'])

TASK [he-yum-installs : install yum packages group] ****************************
Friday 03 May 2019  13:20:14 -0400 (0:00:16.277)       0:03:14.364 ************ 
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]

TASK [he-yum-installs : check for Metacity rpm installed] **********************
Friday 03 May 2019  13:20:14 -0400 (0:00:00.060)       0:03:14.425 ************ 
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf06]
ok: [bcne-wl-pf05]

TASK [he-yum-installs : remove Metacity rpm] ***********************************
Friday 03 May 2019  13:20:16 -0400 (0:00:01.673)       0:03:16.099 ************ 
changed: [bcne-wl-pf06]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]

TASK [he-yum-installs : Update to latest packages] *****************************
Friday 03 May 2019  13:20:17 -0400 (0:00:00.922)       0:03:17.021 ************ 
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]

PLAY [Serial Yum update] *******************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:20:17 -0400 (0:00:00.070)       0:03:17.092 ************ 
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]
ok: [bcne-con-pf01]

TASK [he-yum-internal-repo : Create he_backup directory for repos] *************
Friday 03 May 2019  13:20:17 -0400 (0:00:00.708)       0:03:17.800 ************ 
ok: [bcne-con-pf01]
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]

TASK [he-yum-internal-repo : Copy key file for epel-release] *******************
Friday 03 May 2019  13:20:18 -0400 (0:00:00.404)       0:03:18.205 ************ 
ok: [bcne-con-pf02]
ok: [bcne-con-pf01]
ok: [bcne-lb-pf01]

TASK [he-yum-internal-repo : Turn off plugins in yum.conf] *********************
Friday 03 May 2019  13:20:18 -0400 (0:00:00.778)       0:03:18.983 ************ 
changed: [bcne-lb-pf01]
changed: [bcne-con-pf02]
changed: [bcne-con-pf01]

TASK [he-yum-internal-repo : Turn off external internet repo] ******************
Friday 03 May 2019  13:20:19 -0400 (0:00:00.422)       0:03:19.405 ************ 
changed: [bcne-con-pf01]
changed: [bcne-con-pf02]
changed: [bcne-lb-pf01]

TASK [he-yum-internal-repo : Configure HealthEdge Internal Nexus repo] *********
Friday 03 May 2019  13:20:19 -0400 (0:00:00.389)       0:03:19.795 ************ 
changed: [bcne-con-pf01]
changed: [bcne-con-pf02]
changed: [bcne-lb-pf01]

TASK [he-yum-internal-repo : Configure HealthEdge Internal Nexus EPEL repo] ****
Friday 03 May 2019  13:20:20 -0400 (0:00:00.766)       0:03:20.561 ************ 
changed: [bcne-con-pf01]
changed: [bcne-lb-pf01]
changed: [bcne-con-pf02]

TASK [he-yum-internal-repo : Configure HealthEdge IUS repo] ********************
Friday 03 May 2019  13:20:21 -0400 (0:00:00.770)       0:03:21.332 ************ 
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-yum-internal-repo : Configure HealthEdge Internal IUS repo] ***********
Friday 03 May 2019  13:20:21 -0400 (0:00:00.061)       0:03:21.393 ************ 
changed: [bcne-con-pf01]
changed: [bcne-con-pf02]
changed: [bcne-lb-pf01]

TASK [he-yum-internal-repo : Copy key file for IUS repo] ***********************
Friday 03 May 2019  13:20:22 -0400 (0:00:00.699)       0:03:22.093 ************ 
changed: [bcne-con-pf01]
changed: [bcne-con-pf02]
changed: [bcne-lb-pf01]

TASK [he-yum-installs : install yum packages all] ******************************
Friday 03 May 2019  13:20:22 -0400 (0:00:00.740)       0:03:22.834 ************ 
changed: [bcne-lb-pf01] => (item=[u'tree', u'telnet', u'mutt', u'firewalld', u'rsync', u'unzip'])
changed: [bcne-con-pf01] => (item=[u'tree', u'telnet', u'mutt', u'firewalld', u'rsync', u'unzip'])
changed: [bcne-con-pf02] => (item=[u'tree', u'telnet', u'mutt', u'firewalld', u'rsync', u'unzip'])

TASK [he-yum-installs : install yum packages group] ****************************
Friday 03 May 2019  13:20:41 -0400 (0:00:18.361)       0:03:41.195 ************ 
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
ok: [bcne-lb-pf01] => (item=[u'epel-release'])

TASK [he-yum-installs : check for Metacity rpm installed] **********************
Friday 03 May 2019  13:20:41 -0400 (0:00:00.547)       0:03:41.743 ************ 
ok: [bcne-con-pf01]
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]

TASK [he-yum-installs : remove Metacity rpm] ***********************************
Friday 03 May 2019  13:20:43 -0400 (0:00:01.730)       0:03:43.473 ************ 
changed: [bcne-lb-pf01]
changed: [bcne-con-pf02]
changed: [bcne-con-pf01]

TASK [he-yum-installs : Update to latest packages] *****************************
Friday 03 May 2019  13:20:44 -0400 (0:00:01.094)       0:03:44.568 ************ 
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

PLAY [Install Pre-requisites] **************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:20:44 -0400 (0:00:00.078)       0:03:44.646 ************ 
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf01]
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]

TASK [he-timezone : Configure customer timezone] *******************************
Friday 03 May 2019  13:20:46 -0400 (0:00:01.438)       0:03:46.085 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf06]
changed: [bcne-lb-pf01]
changed: [bcne-con-pf02]
changed: [bcne-con-pf01]

TASK [he-create-sys-app-user : create sys_app_group] ***************************
Friday 03 May 2019  13:20:47 -0400 (0:00:01.061)       0:03:47.147 ************ 
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf06]
changed: [bcne-con-pf02]
changed: [bcne-con-pf01]
changed: [bcne-lb-pf01]

TASK [he-create-sys-app-user : create sys_app_user & user's SSH key] ***********
Friday 03 May 2019  13:20:48 -0400 (0:00:01.155)       0:03:48.302 ************ 
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf01]
changed: [bcne-con-pf01]
changed: [bcne-wl-pf06]
changed: [bcne-lb-pf01]
changed: [bcne-con-pf02]

TASK [he-create-sys-app-user : set perms/ownership sys_app_user home dir] ******
Friday 03 May 2019  13:20:49 -0400 (0:00:01.572)       0:03:49.875 ************ 
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf01]
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]

TASK [he-create-sys-app-user : no group authKey list, set to all] **************
Friday 03 May 2019  13:20:50 -0400 (0:00:00.921)       0:03:50.796 ************ 
skipping: [bcne-wl-pf01]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf01]
ok: [bcne-con-pf02]
ok: [bcne-lb-pf01]

TASK [he-create-sys-app-user : no all authKey list, set to group] **************
Friday 03 May 2019  13:20:51 -0400 (0:00:00.307)       0:03:51.103 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-con-pf01]
skipping: [bcne-lb-pf01]

TASK [he-create-sys-app-user : append generic and group auth key lists] ********
Friday 03 May 2019  13:20:51 -0400 (0:00:00.254)       0:03:51.358 ************ 
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
ok: [bcne-wl-pf01]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-create-sys-app-user : place desired keys in auth keys for user] *******
Friday 03 May 2019  13:20:51 -0400 (0:00:00.193)       0:03:51.551 ************ 
changed: [bcne-wl-pf02] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw8C+73QITcHfH/Zmdi6WbmtxeundwJE81Atay/M3qDVy9/QBfGvq8KX+gWvvVW0iHawWH2LGn1hyLycJTFXjLpXu2zZE6dw1BFjTs1TiUFYb0XOCPSanBy4JikJdk4hXymXnSEWYAp76+/86t3kDD71Ndvhf7833IFre9ctIInwC95MvTM0wtWThFk5InkOi//ott9ul3QPAi8SmK+i1/fahrfJ+/sB4TcXT6KRFeKpq4mGA2ucNL5a/aCAkf13/PsJQt7NYQqq6uYCSfl+UlJ3+CH6f6lV4yVhyf7KMrjdts6xcSnovOYjkdtRCQzZxYL6BZ057dH82H16MauBdgQ== platfralutions@pdcprodinfcc02)
changed: [bcne-wl-pf04] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw8C+73QITcHfH/Zmdi6WbmtxeundwJE81Atay/M3qDVy9/QBfGvq8KX+gWvvVW0iHawWH2LGn1hyLycJTFXjLpXu2zZE6dw1BFjTs1TiUFYb0XOCPSanBy4JikJdk4hXymXnSEWYAp76+/86t3kDD71Ndvhf7833IFre9ctIInwC95MvTM0wtWThFk5InkOi//ott9ul3QPAi8SmK+i1/fahrfJ+/sB4TcXT6KRFeKpq4mGA2ucNL5a/aCAkf13/PsJQt7NYQqq6uYCSfl+UlJ3+CH6f6lV4yVhyf7KMrjdts6xcSnovOYjkdtRCQzZxYL6BZ057dH82H16MauBdgQ== platfralutions@pdcprodinfcc02)
changed: [bcne-wl-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw8C+73QITcHfH/Zmdi6WbmtxeundwJE81Atay/M3qDVy9/QBfGvq8KX+gWvvVW0iHawWH2LGn1hyLycJTFXjLpXu2zZE6dw1BFjTs1TiUFYb0XOCPSanBy4JikJdk4hXymXnSEWYAp76+/86t3kDD71Ndvhf7833IFre9ctIInwC95MvTM0wtWThFk5InkOi//ott9ul3QPAi8SmK+i1/fahrfJ+/sB4TcXT6KRFeKpq4mGA2ucNL5a/aCAkf13/PsJQt7NYQqq6uYCSfl+UlJ3+CH6f6lV4yVhyf7KMrjdts6xcSnovOYjkdtRCQzZxYL6BZ057dH82H16MauBdgQ== platfralutions@pdcprodinfcc02)
changed: [bcne-wl-pf05] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw8C+73QITcHfH/Zmdi6WbmtxeundwJE81Atay/M3qDVy9/QBfGvq8KX+gWvvVW0iHawWH2LGn1hyLycJTFXjLpXu2zZE6dw1BFjTs1TiUFYb0XOCPSanBy4JikJdk4hXymXnSEWYAp76+/86t3kDD71Ndvhf7833IFre9ctIInwC95MvTM0wtWThFk5InkOi//ott9ul3QPAi8SmK+i1/fahrfJ+/sB4TcXT6KRFeKpq4mGA2ucNL5a/aCAkf13/PsJQt7NYQqq6uYCSfl+UlJ3+CH6f6lV4yVhyf7KMrjdts6xcSnovOYjkdtRCQzZxYL6BZ057dH82H16MauBdgQ== platfralutions@pdcprodinfcc02)
changed: [bcne-wl-pf03] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw8C+73QITcHfH/Zmdi6WbmtxeundwJE81Atay/M3qDVy9/QBfGvq8KX+gWvvVW0iHawWH2LGn1hyLycJTFXjLpXu2zZE6dw1BFjTs1TiUFYb0XOCPSanBy4JikJdk4hXymXnSEWYAp76+/86t3kDD71Ndvhf7833IFre9ctIInwC95MvTM0wtWThFk5InkOi//ott9ul3QPAi8SmK+i1/fahrfJ+/sB4TcXT6KRFeKpq4mGA2ucNL5a/aCAkf13/PsJQt7NYQqq6uYCSfl+UlJ3+CH6f6lV4yVhyf7KMrjdts6xcSnovOYjkdtRCQzZxYL6BZ057dH82H16MauBdgQ== platfralutions@pdcprodinfcc02)
changed: [bcne-wl-pf02] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArGh4lwnZkUYRtCAZhVR9wpfZfBwuke+A1NBfNv8hoDXQhMdJI3+0BVKr6f7QUDgKU5koPQnBZofsmQbzuG0YexaICMsBvM/qnQqvgZhehEiIafXF7zti9pYJoAGmOny5Sm4Uf5ZTWlI67EO5i/vDHkxy6wp+Xx7qI8D5Oge/mVJqakhZ2CYc25gbpfV+3qHE+woqBQIJYl1we26IkIzw/DlimKIl8xpaQR8T2moqsRC5F+EUbJrWC1XAXC73uHV2gw9UdT2K7+tATW0mhkxqqV/zBqP3ws+90yONYoE3XaDnucravEHTt3kpxDI+Or7iNObRaKkIBIU02qG/lGneOQ== smcilvenna@pdcprodinfcc02)
changed: [bcne-wl-pf04] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArGh4lwnZkUYRtCAZhVR9wpfZfBwuke+A1NBfNv8hoDXQhMdJI3+0BVKr6f7QUDgKU5koPQnBZofsmQbzuG0YexaICMsBvM/qnQqvgZhehEiIafXF7zti9pYJoAGmOny5Sm4Uf5ZTWlI67EO5i/vDHkxy6wp+Xx7qI8D5Oge/mVJqakhZ2CYc25gbpfV+3qHE+woqBQIJYl1we26IkIzw/DlimKIl8xpaQR8T2moqsRC5F+EUbJrWC1XAXC73uHV2gw9UdT2K7+tATW0mhkxqqV/zBqP3ws+90yONYoE3XaDnucravEHTt3kpxDI+Or7iNObRaKkIBIU02qG/lGneOQ== smcilvenna@pdcprodinfcc02)
changed: [bcne-wl-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArGh4lwnZkUYRtCAZhVR9wpfZfBwuke+A1NBfNv8hoDXQhMdJI3+0BVKr6f7QUDgKU5koPQnBZofsmQbzuG0YexaICMsBvM/qnQqvgZhehEiIafXF7zti9pYJoAGmOny5Sm4Uf5ZTWlI67EO5i/vDHkxy6wp+Xx7qI8D5Oge/mVJqakhZ2CYc25gbpfV+3qHE+woqBQIJYl1we26IkIzw/DlimKIl8xpaQR8T2moqsRC5F+EUbJrWC1XAXC73uHV2gw9UdT2K7+tATW0mhkxqqV/zBqP3ws+90yONYoE3XaDnucravEHTt3kpxDI+Or7iNObRaKkIBIU02qG/lGneOQ== smcilvenna@pdcprodinfcc02)
changed: [bcne-wl-pf03] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArGh4lwnZkUYRtCAZhVR9wpfZfBwuke+A1NBfNv8hoDXQhMdJI3+0BVKr6f7QUDgKU5koPQnBZofsmQbzuG0YexaICMsBvM/qnQqvgZhehEiIafXF7zti9pYJoAGmOny5Sm4Uf5ZTWlI67EO5i/vDHkxy6wp+Xx7qI8D5Oge/mVJqakhZ2CYc25gbpfV+3qHE+woqBQIJYl1we26IkIzw/DlimKIl8xpaQR8T2moqsRC5F+EUbJrWC1XAXC73uHV2gw9UdT2K7+tATW0mhkxqqV/zBqP3ws+90yONYoE3XaDnucravEHTt3kpxDI+Or7iNObRaKkIBIU02qG/lGneOQ== smcilvenna@pdcprodinfcc02)
changed: [bcne-wl-pf05] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArGh4lwnZkUYRtCAZhVR9wpfZfBwuke+A1NBfNv8hoDXQhMdJI3+0BVKr6f7QUDgKU5koPQnBZofsmQbzuG0YexaICMsBvM/qnQqvgZhehEiIafXF7zti9pYJoAGmOny5Sm4Uf5ZTWlI67EO5i/vDHkxy6wp+Xx7qI8D5Oge/mVJqakhZ2CYc25gbpfV+3qHE+woqBQIJYl1we26IkIzw/DlimKIl8xpaQR8T2moqsRC5F+EUbJrWC1XAXC73uHV2gw9UdT2K7+tATW0mhkxqqV/zBqP3ws+90yONYoE3XaDnucravEHTt3kpxDI+Or7iNObRaKkIBIU02qG/lGneOQ== smcilvenna@pdcprodinfcc02)
changed: [bcne-wl-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzTHirhmfq8PreOHoAwuMxnYbh3uB7o/BJczRX9Etfl9LTCu7lNSJ5tspQGp4L89vyEtloYURNWMuLHBPIRleG/P6C5DGiNFLNrmaY8irZh/MbTvNs5JnLRgJm08zd5mkvuvT6q1Z7A5xGNx1pyAkoNUJAP5yy569ONg3lJEb4O+MzQag0UlZqqx8LvM144hZP9V0s36DTJr5HCCriZtAhAsw+NmhcDqPBDuSWGrVDi0k8huyCvGj8my3FMaZcokHBHow6rJHke/sYPSZfWbPGqpOwV6B578u3Q3+/D/+jdvjZSe8Ili+F4i8BRYwXVw1zO4ouSIMpZAvB48ub8F2Xw== ansible@he2-auto-01)
changed: [bcne-wl-pf04] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzTHirhmfq8PreOHoAwuMxnYbh3uB7o/BJczRX9Etfl9LTCu7lNSJ5tspQGp4L89vyEtloYURNWMuLHBPIRleG/P6C5DGiNFLNrmaY8irZh/MbTvNs5JnLRgJm08zd5mkvuvT6q1Z7A5xGNx1pyAkoNUJAP5yy569ONg3lJEb4O+MzQag0UlZqqx8LvM144hZP9V0s36DTJr5HCCriZtAhAsw+NmhcDqPBDuSWGrVDi0k8huyCvGj8my3FMaZcokHBHow6rJHke/sYPSZfWbPGqpOwV6B578u3Q3+/D/+jdvjZSe8Ili+F4i8BRYwXVw1zO4ouSIMpZAvB48ub8F2Xw== ansible@he2-auto-01)
changed: [bcne-wl-pf02] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzTHirhmfq8PreOHoAwuMxnYbh3uB7o/BJczRX9Etfl9LTCu7lNSJ5tspQGp4L89vyEtloYURNWMuLHBPIRleG/P6C5DGiNFLNrmaY8irZh/MbTvNs5JnLRgJm08zd5mkvuvT6q1Z7A5xGNx1pyAkoNUJAP5yy569ONg3lJEb4O+MzQag0UlZqqx8LvM144hZP9V0s36DTJr5HCCriZtAhAsw+NmhcDqPBDuSWGrVDi0k8huyCvGj8my3FMaZcokHBHow6rJHke/sYPSZfWbPGqpOwV6B578u3Q3+/D/+jdvjZSe8Ili+F4i8BRYwXVw1zO4ouSIMpZAvB48ub8F2Xw== ansible@he2-auto-01)
changed: [bcne-wl-pf05] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzTHirhmfq8PreOHoAwuMxnYbh3uB7o/BJczRX9Etfl9LTCu7lNSJ5tspQGp4L89vyEtloYURNWMuLHBPIRleG/P6C5DGiNFLNrmaY8irZh/MbTvNs5JnLRgJm08zd5mkvuvT6q1Z7A5xGNx1pyAkoNUJAP5yy569ONg3lJEb4O+MzQag0UlZqqx8LvM144hZP9V0s36DTJr5HCCriZtAhAsw+NmhcDqPBDuSWGrVDi0k8huyCvGj8my3FMaZcokHBHow6rJHke/sYPSZfWbPGqpOwV6B578u3Q3+/D/+jdvjZSe8Ili+F4i8BRYwXVw1zO4ouSIMpZAvB48ub8F2Xw== ansible@he2-auto-01)
changed: [bcne-wl-pf03] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzTHirhmfq8PreOHoAwuMxnYbh3uB7o/BJczRX9Etfl9LTCu7lNSJ5tspQGp4L89vyEtloYURNWMuLHBPIRleG/P6C5DGiNFLNrmaY8irZh/MbTvNs5JnLRgJm08zd5mkvuvT6q1Z7A5xGNx1pyAkoNUJAP5yy569ONg3lJEb4O+MzQag0UlZqqx8LvM144hZP9V0s36DTJr5HCCriZtAhAsw+NmhcDqPBDuSWGrVDi0k8huyCvGj8my3FMaZcokHBHow6rJHke/sYPSZfWbPGqpOwV6B578u3Q3+/D/+jdvjZSe8Ili+F4i8BRYwXVw1zO4ouSIMpZAvB48ub8F2Xw== ansible@he2-auto-01)
changed: [bcne-wl-pf04] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA21es5Jbvz7jY8gWrHV7d3mE6U1p4joPoyP06wzgavq7s80/vHANos/c4jGnVHHMPmRuMw0odJ+pLVg1ukswwGZwlNztxFduxEG0a4R2oLKI04zV67Z7ke8CKwWLZQtqFKNvXtv24pR7ccr7m6BYkkAufivStN0HkYBVKfhZ7PWSCFUcIW6crwxi/HNCsGhUAhvVrSv9zM04rWYXcT4uapTwqMOowN41tuJxgwR0Md0U3d7y4xGesKmc3KWwYa1xOq8JNknj+tX/y9V8vCoPwbSoAL7nMaV6O62HmyRp+TTvj+wkoLOPoV3M8UYk/5Eu+G1GM0BAAIZa2PGtwrI6dcQ== ansible@HE1-AUTO-01)
changed: [bcne-wl-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA21es5Jbvz7jY8gWrHV7d3mE6U1p4joPoyP06wzgavq7s80/vHANos/c4jGnVHHMPmRuMw0odJ+pLVg1ukswwGZwlNztxFduxEG0a4R2oLKI04zV67Z7ke8CKwWLZQtqFKNvXtv24pR7ccr7m6BYkkAufivStN0HkYBVKfhZ7PWSCFUcIW6crwxi/HNCsGhUAhvVrSv9zM04rWYXcT4uapTwqMOowN41tuJxgwR0Md0U3d7y4xGesKmc3KWwYa1xOq8JNknj+tX/y9V8vCoPwbSoAL7nMaV6O62HmyRp+TTvj+wkoLOPoV3M8UYk/5Eu+G1GM0BAAIZa2PGtwrI6dcQ== ansible@HE1-AUTO-01)
changed: [bcne-wl-pf02] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA21es5Jbvz7jY8gWrHV7d3mE6U1p4joPoyP06wzgavq7s80/vHANos/c4jGnVHHMPmRuMw0odJ+pLVg1ukswwGZwlNztxFduxEG0a4R2oLKI04zV67Z7ke8CKwWLZQtqFKNvXtv24pR7ccr7m6BYkkAufivStN0HkYBVKfhZ7PWSCFUcIW6crwxi/HNCsGhUAhvVrSv9zM04rWYXcT4uapTwqMOowN41tuJxgwR0Md0U3d7y4xGesKmc3KWwYa1xOq8JNknj+tX/y9V8vCoPwbSoAL7nMaV6O62HmyRp+TTvj+wkoLOPoV3M8UYk/5Eu+G1GM0BAAIZa2PGtwrI6dcQ== ansible@HE1-AUTO-01)
changed: [bcne-wl-pf05] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA21es5Jbvz7jY8gWrHV7d3mE6U1p4joPoyP06wzgavq7s80/vHANos/c4jGnVHHMPmRuMw0odJ+pLVg1ukswwGZwlNztxFduxEG0a4R2oLKI04zV67Z7ke8CKwWLZQtqFKNvXtv24pR7ccr7m6BYkkAufivStN0HkYBVKfhZ7PWSCFUcIW6crwxi/HNCsGhUAhvVrSv9zM04rWYXcT4uapTwqMOowN41tuJxgwR0Md0U3d7y4xGesKmc3KWwYa1xOq8JNknj+tX/y9V8vCoPwbSoAL7nMaV6O62HmyRp+TTvj+wkoLOPoV3M8UYk/5Eu+G1GM0BAAIZa2PGtwrI6dcQ== ansible@HE1-AUTO-01)
changed: [bcne-wl-pf03] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA21es5Jbvz7jY8gWrHV7d3mE6U1p4joPoyP06wzgavq7s80/vHANos/c4jGnVHHMPmRuMw0odJ+pLVg1ukswwGZwlNztxFduxEG0a4R2oLKI04zV67Z7ke8CKwWLZQtqFKNvXtv24pR7ccr7m6BYkkAufivStN0HkYBVKfhZ7PWSCFUcIW6crwxi/HNCsGhUAhvVrSv9zM04rWYXcT4uapTwqMOowN41tuJxgwR0Md0U3d7y4xGesKmc3KWwYa1xOq8JNknj+tX/y9V8vCoPwbSoAL7nMaV6O62HmyRp+TTvj+wkoLOPoV3M8UYk/5Eu+G1GM0BAAIZa2PGtwrI6dcQ== ansible@HE1-AUTO-01)
ok: [bcne-wl-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA21es5Jbvz7jY8gWrHV7d3mE6U1p4joPoyP06wzgavq7s80/vHANos/c4jGnVHHMPmRuMw0odJ+pLVg1ukswwGZwlNztxFduxEG0a4R2oLKI04zV67Z7ke8CKwWLZQtqFKNvXtv24pR7ccr7m6BYkkAufivStN0HkYBVKfhZ7PWSCFUcIW6crwxi/HNCsGhUAhvVrSv9zM04rWYXcT4uapTwqMOowN41tuJxgwR0Md0U3d7y4xGesKmc3KWwYa1xOq8JNknj+tX/y9V8vCoPwbSoAL7nMaV6O62HmyRp+TTvj+wkoLOPoV3M8UYk/5Eu+G1GM0BAAIZa2PGtwrI6dcQ== ansible@HE1-AUTO-01)
changed: [bcne-wl-pf06] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw8C+73QITcHfH/Zmdi6WbmtxeundwJE81Atay/M3qDVy9/QBfGvq8KX+gWvvVW0iHawWH2LGn1hyLycJTFXjLpXu2zZE6dw1BFjTs1TiUFYb0XOCPSanBy4JikJdk4hXymXnSEWYAp76+/86t3kDD71Ndvhf7833IFre9ctIInwC95MvTM0wtWThFk5InkOi//ott9ul3QPAi8SmK+i1/fahrfJ+/sB4TcXT6KRFeKpq4mGA2ucNL5a/aCAkf13/PsJQt7NYQqq6uYCSfl+UlJ3+CH6f6lV4yVhyf7KMrjdts6xcSnovOYjkdtRCQzZxYL6BZ057dH82H16MauBdgQ== platfralutions@pdcprodinfcc02)
changed: [bcne-con-pf02] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw8C+73QITcHfH/Zmdi6WbmtxeundwJE81Atay/M3qDVy9/QBfGvq8KX+gWvvVW0iHawWH2LGn1hyLycJTFXjLpXu2zZE6dw1BFjTs1TiUFYb0XOCPSanBy4JikJdk4hXymXnSEWYAp76+/86t3kDD71Ndvhf7833IFre9ctIInwC95MvTM0wtWThFk5InkOi//ott9ul3QPAi8SmK+i1/fahrfJ+/sB4TcXT6KRFeKpq4mGA2ucNL5a/aCAkf13/PsJQt7NYQqq6uYCSfl+UlJ3+CH6f6lV4yVhyf7KMrjdts6xcSnovOYjkdtRCQzZxYL6BZ057dH82H16MauBdgQ== platfralutions@pdcprodinfcc02)
changed: [bcne-con-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw8C+73QITcHfH/Zmdi6WbmtxeundwJE81Atay/M3qDVy9/QBfGvq8KX+gWvvVW0iHawWH2LGn1hyLycJTFXjLpXu2zZE6dw1BFjTs1TiUFYb0XOCPSanBy4JikJdk4hXymXnSEWYAp76+/86t3kDD71Ndvhf7833IFre9ctIInwC95MvTM0wtWThFk5InkOi//ott9ul3QPAi8SmK+i1/fahrfJ+/sB4TcXT6KRFeKpq4mGA2ucNL5a/aCAkf13/PsJQt7NYQqq6uYCSfl+UlJ3+CH6f6lV4yVhyf7KMrjdts6xcSnovOYjkdtRCQzZxYL6BZ057dH82H16MauBdgQ== platfralutions@pdcprodinfcc02)
changed: [bcne-lb-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAw8C+73QITcHfH/Zmdi6WbmtxeundwJE81Atay/M3qDVy9/QBfGvq8KX+gWvvVW0iHawWH2LGn1hyLycJTFXjLpXu2zZE6dw1BFjTs1TiUFYb0XOCPSanBy4JikJdk4hXymXnSEWYAp76+/86t3kDD71Ndvhf7833IFre9ctIInwC95MvTM0wtWThFk5InkOi//ott9ul3QPAi8SmK+i1/fahrfJ+/sB4TcXT6KRFeKpq4mGA2ucNL5a/aCAkf13/PsJQt7NYQqq6uYCSfl+UlJ3+CH6f6lV4yVhyf7KMrjdts6xcSnovOYjkdtRCQzZxYL6BZ057dH82H16MauBdgQ== platfralutions@pdcprodinfcc02)
changed: [bcne-wl-pf06] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArGh4lwnZkUYRtCAZhVR9wpfZfBwuke+A1NBfNv8hoDXQhMdJI3+0BVKr6f7QUDgKU5koPQnBZofsmQbzuG0YexaICMsBvM/qnQqvgZhehEiIafXF7zti9pYJoAGmOny5Sm4Uf5ZTWlI67EO5i/vDHkxy6wp+Xx7qI8D5Oge/mVJqakhZ2CYc25gbpfV+3qHE+woqBQIJYl1we26IkIzw/DlimKIl8xpaQR8T2moqsRC5F+EUbJrWC1XAXC73uHV2gw9UdT2K7+tATW0mhkxqqV/zBqP3ws+90yONYoE3XaDnucravEHTt3kpxDI+Or7iNObRaKkIBIU02qG/lGneOQ== smcilvenna@pdcprodinfcc02)
changed: [bcne-con-pf02] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArGh4lwnZkUYRtCAZhVR9wpfZfBwuke+A1NBfNv8hoDXQhMdJI3+0BVKr6f7QUDgKU5koPQnBZofsmQbzuG0YexaICMsBvM/qnQqvgZhehEiIafXF7zti9pYJoAGmOny5Sm4Uf5ZTWlI67EO5i/vDHkxy6wp+Xx7qI8D5Oge/mVJqakhZ2CYc25gbpfV+3qHE+woqBQIJYl1we26IkIzw/DlimKIl8xpaQR8T2moqsRC5F+EUbJrWC1XAXC73uHV2gw9UdT2K7+tATW0mhkxqqV/zBqP3ws+90yONYoE3XaDnucravEHTt3kpxDI+Or7iNObRaKkIBIU02qG/lGneOQ== smcilvenna@pdcprodinfcc02)
changed: [bcne-wl-pf06] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzTHirhmfq8PreOHoAwuMxnYbh3uB7o/BJczRX9Etfl9LTCu7lNSJ5tspQGp4L89vyEtloYURNWMuLHBPIRleG/P6C5DGiNFLNrmaY8irZh/MbTvNs5JnLRgJm08zd5mkvuvT6q1Z7A5xGNx1pyAkoNUJAP5yy569ONg3lJEb4O+MzQag0UlZqqx8LvM144hZP9V0s36DTJr5HCCriZtAhAsw+NmhcDqPBDuSWGrVDi0k8huyCvGj8my3FMaZcokHBHow6rJHke/sYPSZfWbPGqpOwV6B578u3Q3+/D/+jdvjZSe8Ili+F4i8BRYwXVw1zO4ouSIMpZAvB48ub8F2Xw== ansible@he2-auto-01)
changed: [bcne-lb-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArGh4lwnZkUYRtCAZhVR9wpfZfBwuke+A1NBfNv8hoDXQhMdJI3+0BVKr6f7QUDgKU5koPQnBZofsmQbzuG0YexaICMsBvM/qnQqvgZhehEiIafXF7zti9pYJoAGmOny5Sm4Uf5ZTWlI67EO5i/vDHkxy6wp+Xx7qI8D5Oge/mVJqakhZ2CYc25gbpfV+3qHE+woqBQIJYl1we26IkIzw/DlimKIl8xpaQR8T2moqsRC5F+EUbJrWC1XAXC73uHV2gw9UdT2K7+tATW0mhkxqqV/zBqP3ws+90yONYoE3XaDnucravEHTt3kpxDI+Or7iNObRaKkIBIU02qG/lGneOQ== smcilvenna@pdcprodinfcc02)
changed: [bcne-con-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEArGh4lwnZkUYRtCAZhVR9wpfZfBwuke+A1NBfNv8hoDXQhMdJI3+0BVKr6f7QUDgKU5koPQnBZofsmQbzuG0YexaICMsBvM/qnQqvgZhehEiIafXF7zti9pYJoAGmOny5Sm4Uf5ZTWlI67EO5i/vDHkxy6wp+Xx7qI8D5Oge/mVJqakhZ2CYc25gbpfV+3qHE+woqBQIJYl1we26IkIzw/DlimKIl8xpaQR8T2moqsRC5F+EUbJrWC1XAXC73uHV2gw9UdT2K7+tATW0mhkxqqV/zBqP3ws+90yONYoE3XaDnucravEHTt3kpxDI+Or7iNObRaKkIBIU02qG/lGneOQ== smcilvenna@pdcprodinfcc02)
changed: [bcne-con-pf02] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzTHirhmfq8PreOHoAwuMxnYbh3uB7o/BJczRX9Etfl9LTCu7lNSJ5tspQGp4L89vyEtloYURNWMuLHBPIRleG/P6C5DGiNFLNrmaY8irZh/MbTvNs5JnLRgJm08zd5mkvuvT6q1Z7A5xGNx1pyAkoNUJAP5yy569ONg3lJEb4O+MzQag0UlZqqx8LvM144hZP9V0s36DTJr5HCCriZtAhAsw+NmhcDqPBDuSWGrVDi0k8huyCvGj8my3FMaZcokHBHow6rJHke/sYPSZfWbPGqpOwV6B578u3Q3+/D/+jdvjZSe8Ili+F4i8BRYwXVw1zO4ouSIMpZAvB48ub8F2Xw== ansible@he2-auto-01)
changed: [bcne-lb-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzTHirhmfq8PreOHoAwuMxnYbh3uB7o/BJczRX9Etfl9LTCu7lNSJ5tspQGp4L89vyEtloYURNWMuLHBPIRleG/P6C5DGiNFLNrmaY8irZh/MbTvNs5JnLRgJm08zd5mkvuvT6q1Z7A5xGNx1pyAkoNUJAP5yy569ONg3lJEb4O+MzQag0UlZqqx8LvM144hZP9V0s36DTJr5HCCriZtAhAsw+NmhcDqPBDuSWGrVDi0k8huyCvGj8my3FMaZcokHBHow6rJHke/sYPSZfWbPGqpOwV6B578u3Q3+/D/+jdvjZSe8Ili+F4i8BRYwXVw1zO4ouSIMpZAvB48ub8F2Xw== ansible@he2-auto-01)
changed: [bcne-wl-pf06] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA21es5Jbvz7jY8gWrHV7d3mE6U1p4joPoyP06wzgavq7s80/vHANos/c4jGnVHHMPmRuMw0odJ+pLVg1ukswwGZwlNztxFduxEG0a4R2oLKI04zV67Z7ke8CKwWLZQtqFKNvXtv24pR7ccr7m6BYkkAufivStN0HkYBVKfhZ7PWSCFUcIW6crwxi/HNCsGhUAhvVrSv9zM04rWYXcT4uapTwqMOowN41tuJxgwR0Md0U3d7y4xGesKmc3KWwYa1xOq8JNknj+tX/y9V8vCoPwbSoAL7nMaV6O62HmyRp+TTvj+wkoLOPoV3M8UYk/5Eu+G1GM0BAAIZa2PGtwrI6dcQ== ansible@HE1-AUTO-01)
changed: [bcne-con-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzTHirhmfq8PreOHoAwuMxnYbh3uB7o/BJczRX9Etfl9LTCu7lNSJ5tspQGp4L89vyEtloYURNWMuLHBPIRleG/P6C5DGiNFLNrmaY8irZh/MbTvNs5JnLRgJm08zd5mkvuvT6q1Z7A5xGNx1pyAkoNUJAP5yy569ONg3lJEb4O+MzQag0UlZqqx8LvM144hZP9V0s36DTJr5HCCriZtAhAsw+NmhcDqPBDuSWGrVDi0k8huyCvGj8my3FMaZcokHBHow6rJHke/sYPSZfWbPGqpOwV6B578u3Q3+/D/+jdvjZSe8Ili+F4i8BRYwXVw1zO4ouSIMpZAvB48ub8F2Xw== ansible@he2-auto-01)
changed: [bcne-con-pf02] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA21es5Jbvz7jY8gWrHV7d3mE6U1p4joPoyP06wzgavq7s80/vHANos/c4jGnVHHMPmRuMw0odJ+pLVg1ukswwGZwlNztxFduxEG0a4R2oLKI04zV67Z7ke8CKwWLZQtqFKNvXtv24pR7ccr7m6BYkkAufivStN0HkYBVKfhZ7PWSCFUcIW6crwxi/HNCsGhUAhvVrSv9zM04rWYXcT4uapTwqMOowN41tuJxgwR0Md0U3d7y4xGesKmc3KWwYa1xOq8JNknj+tX/y9V8vCoPwbSoAL7nMaV6O62HmyRp+TTvj+wkoLOPoV3M8UYk/5Eu+G1GM0BAAIZa2PGtwrI6dcQ== ansible@HE1-AUTO-01)
changed: [bcne-lb-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA21es5Jbvz7jY8gWrHV7d3mE6U1p4joPoyP06wzgavq7s80/vHANos/c4jGnVHHMPmRuMw0odJ+pLVg1ukswwGZwlNztxFduxEG0a4R2oLKI04zV67Z7ke8CKwWLZQtqFKNvXtv24pR7ccr7m6BYkkAufivStN0HkYBVKfhZ7PWSCFUcIW6crwxi/HNCsGhUAhvVrSv9zM04rWYXcT4uapTwqMOowN41tuJxgwR0Md0U3d7y4xGesKmc3KWwYa1xOq8JNknj+tX/y9V8vCoPwbSoAL7nMaV6O62HmyRp+TTvj+wkoLOPoV3M8UYk/5Eu+G1GM0BAAIZa2PGtwrI6dcQ== ansible@HE1-AUTO-01)
changed: [bcne-con-pf01] => (item=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA21es5Jbvz7jY8gWrHV7d3mE6U1p4joPoyP06wzgavq7s80/vHANos/c4jGnVHHMPmRuMw0odJ+pLVg1ukswwGZwlNztxFduxEG0a4R2oLKI04zV67Z7ke8CKwWLZQtqFKNvXtv24pR7ccr7m6BYkkAufivStN0HkYBVKfhZ7PWSCFUcIW6crwxi/HNCsGhUAhvVrSv9zM04rWYXcT4uapTwqMOowN41tuJxgwR0Md0U3d7y4xGesKmc3KWwYa1xOq8JNknj+tX/y9V8vCoPwbSoAL7nMaV6O62HmyRp+TTvj+wkoLOPoV3M8UYk/5Eu+G1GM0BAAIZa2PGtwrI6dcQ== ansible@HE1-AUTO-01)

TASK [he-create-sys-app-user : Set authorized key copying it from current ansible user on current provisioning server] ***
Friday 03 May 2019  13:20:56 -0400 (0:00:04.561)       0:03:56.112 ************ 
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf02]
ok: [bcne-con-pf01]
ok: [bcne-lb-pf01]

TASK [he-firewalld : Make sure firewall is started] ****************************
Friday 03 May 2019  13:20:57 -0400 (0:00:01.198)       0:03:57.311 ************ 
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf01]
ok: [bcne-lb-pf01]
ok: [bcne-con-pf02]

TASK [he-firewalld : Open firewalld ports from all list] ***********************
Friday 03 May 2019  13:20:58 -0400 (0:00:01.321)       0:03:58.633 ************ 
changed: [bcne-wl-pf01] => (item=3872/tcp)
changed: [bcne-wl-pf05] => (item=3872/tcp)
changed: [bcne-wl-pf03] => (item=3872/tcp)
changed: [bcne-wl-pf02] => (item=3872/tcp)
changed: [bcne-wl-pf04] => (item=3872/tcp)
changed: [bcne-wl-pf01] => (item=22/tcp)
changed: [bcne-wl-pf02] => (item=22/tcp)
changed: [bcne-wl-pf05] => (item=22/tcp)
changed: [bcne-wl-pf03] => (item=22/tcp)
changed: [bcne-wl-pf04] => (item=22/tcp)
changed: [bcne-wl-pf06] => (item=3872/tcp)
changed: [bcne-lb-pf01] => (item=3872/tcp)
changed: [bcne-con-pf02] => (item=3872/tcp)
changed: [bcne-con-pf01] => (item=3872/tcp)
changed: [bcne-wl-pf06] => (item=22/tcp)
changed: [bcne-con-pf02] => (item=22/tcp)
changed: [bcne-lb-pf01] => (item=22/tcp)
changed: [bcne-con-pf01] => (item=22/tcp)

TASK [he-firewalld : Open firewalld ports from group list] *********************
Friday 03 May 2019  13:21:02 -0400 (0:00:03.407)       0:04:02.040 ************ 
changed: [bcne-wl-pf01] => (item=3001-3003/tcp)
changed: [bcne-wl-pf03] => (item=3001-4903/tcp)
changed: [bcne-wl-pf02] => (item=3001-4903/tcp)
changed: [bcne-wl-pf04] => (item=3001-4903/tcp)
changed: [bcne-wl-pf05] => (item=3001-4903/tcp)
changed: [bcne-wl-pf01] => (item=3011-4903/tcp)
changed: [bcne-wl-pf02] => (item=10001/tcp)
changed: [bcne-wl-pf05] => (item=10001/tcp)
changed: [bcne-wl-pf04] => (item=10001/tcp)
changed: [bcne-wl-pf03] => (item=10001/tcp)
changed: [bcne-wl-pf02] => (item=30000-40000/tcp)
changed: [bcne-wl-pf01] => (item=8001-8003/tcp)
changed: [bcne-wl-pf05] => (item=30000-40000/tcp)
changed: [bcne-wl-pf04] => (item=30000-40000/tcp)
changed: [bcne-wl-pf03] => (item=30000-40000/tcp)
changed: [bcne-wl-pf01] => (item=9001-9003/tcp)
changed: [bcne-wl-pf06] => (item=3001-4903/tcp)
changed: [bcne-lb-pf01] => (item=5555-5565/tcp)
changed: [bcne-con-pf02] => (item=8181/tcp)
changed: [bcne-con-pf01] => (item=8181/tcp)
changed: [bcne-wl-pf01] => (item=10001-10003/tcp)
changed: [bcne-wl-pf06] => (item=10001/tcp)
changed: [bcne-con-pf02] => (item=8101/tcp)
changed: [bcne-con-pf01] => (item=8101/tcp)
changed: [bcne-wl-pf01] => (item=30000-40000/tcp)
changed: [bcne-wl-pf06] => (item=30000-40000/tcp)
changed: [bcne-con-pf02] => (item=9191-9193/tcp)
changed: [bcne-con-pf01] => (item=9191-9193/tcp)
changed: [bcne-con-pf02] => (item=9200/tcp)
changed: [bcne-con-pf01] => (item=9200/tcp)

TASK [he-firewalld : Bounce firewalld] *****************************************
Friday 03 May 2019  13:21:07 -0400 (0:00:05.188)       0:04:07.228 ************ 
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]
changed: [bcne-lb-pf01]
changed: [bcne-wl-pf06]
changed: [bcne-con-pf02]
changed: [bcne-con-pf01]

TASK [he-access-conf : add access.conf disable all line] ***********************
Friday 03 May 2019  13:21:10 -0400 (0:00:02.991)       0:04:10.220 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf06]
changed: [bcne-lb-pf01]
changed: [bcne-con-pf01]
changed: [bcne-con-pf02]

TASK [he-access-conf : add access.conf whitelist all lines] ********************
Friday 03 May 2019  13:21:11 -0400 (0:00:01.030)       0:04:11.250 ************ 
changed: [bcne-wl-pf03] => (item=+ : ansible  : 10.10.14.0/23)
changed: [bcne-wl-pf04] => (item=+ : ansible  : 10.10.14.0/23)
changed: [bcne-wl-pf01] => (item=+ : ansible  : 10.10.14.0/23)
changed: [bcne-wl-pf02] => (item=+ : ansible  : 10.10.14.0/23)
changed: [bcne-wl-pf05] => (item=+ : ansible  : 10.10.14.0/23)
changed: [bcne-wl-pf01] => (item=+ : ansible  : 10.11.14.0/23)
changed: [bcne-wl-pf03] => (item=+ : ansible  : 10.11.14.0/23)
changed: [bcne-wl-pf04] => (item=+ : ansible  : 10.11.14.0/23)
changed: [bcne-wl-pf02] => (item=+ : ansible  : 10.11.14.0/23)
changed: [bcne-wl-pf05] => (item=+ : ansible  : 10.11.14.0/23)
changed: [bcne-wl-pf03] => (item=+ : weblogic      : 10.10.14.0/23)
changed: [bcne-wl-pf04] => (item=+ : weblogic      : 10.10.14.0/23)
changed: [bcne-wl-pf05] => (item=+ : weblogic      : 10.10.14.0/23)
changed: [bcne-wl-pf02] => (item=+ : weblogic      : 10.10.14.0/23)
changed: [bcne-wl-pf01] => (item=+ : weblogic      : 10.10.14.0/23)
changed: [bcne-wl-pf02] => (item=+ : weblogic      : 10.11.14.0/23)
changed: [bcne-wl-pf04] => (item=+ : weblogic      : 10.11.14.0/23)
changed: [bcne-wl-pf03] => (item=+ : weblogic      : 10.11.14.0/23)
changed: [bcne-wl-pf01] => (item=+ : weblogic      : 10.11.14.0/23)
changed: [bcne-wl-pf05] => (item=+ : weblogic      : 10.11.14.0/23)
changed: [bcne-wl-pf04] => (item=+ : weblogic      : cron crond : 0)
changed: [bcne-wl-pf02] => (item=+ : weblogic      : cron crond : 0)
changed: [bcne-wl-pf01] => (item=+ : weblogic      : cron crond : 0)
changed: [bcne-wl-pf05] => (item=+ : weblogic      : cron crond : 0)
changed: [bcne-wl-pf03] => (item=+ : weblogic      : cron crond : 0)
changed: [bcne-con-pf02] => (item=+ : ansible  : 10.10.14.0/23)
changed: [bcne-wl-pf06] => (item=+ : ansible  : 10.10.14.0/23)
changed: [bcne-con-pf01] => (item=+ : ansible  : 10.10.14.0/23)
changed: [bcne-lb-pf01] => (item=+ : ansible  : 10.10.14.0/23)
changed: [bcne-con-pf02] => (item=+ : ansible  : 10.11.14.0/23)
changed: [bcne-wl-pf06] => (item=+ : ansible  : 10.11.14.0/23)
changed: [bcne-con-pf01] => (item=+ : ansible  : 10.11.14.0/23)
changed: [bcne-lb-pf01] => (item=+ : ansible  : 10.11.14.0/23)
changed: [bcne-wl-pf06] => (item=+ : weblogic      : 10.10.14.0/23)
changed: [bcne-lb-pf01] => (item=+ : httpd      : 10.10.14.0/23)
changed: [bcne-con-pf02] => (item=+ : cprime      : 10.10.14.0/23)
changed: [bcne-con-pf01] => (item=+ : cprime      : 10.10.14.0/23)
changed: [bcne-lb-pf01] => (item=+ : httpd      : 10.11.14.0/23)
changed: [bcne-con-pf01] => (item=+ : cprime      : 10.11.14.0/23)
changed: [bcne-wl-pf06] => (item=+ : weblogic      : 10.11.14.0/23)
changed: [bcne-con-pf02] => (item=+ : cprime      : 10.11.14.0/23)
changed: [bcne-con-pf01] => (item=+ : cprime      : cron crond : 0)
changed: [bcne-con-pf02] => (item=+ : cprime      : cron crond : 0)
changed: [bcne-lb-pf01] => (item=+ : httpd      : cron crond : 0)
changed: [bcne-wl-pf06] => (item=+ : weblogic      : cron crond : 0)

TASK [he-access-conf : add access.conf whitelist group lines] ******************
Friday 03 May 2019  13:21:16 -0400 (0:00:04.807)       0:04:16.057 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-limits-conf : add limits.conf from all list] **************************
Friday 03 May 2019  13:21:16 -0400 (0:00:00.276)       0:04:16.334 ************ 
changed: [bcne-wl-pf01] => (item=weblogic      hard   nofile   500000)
changed: [bcne-wl-pf03] => (item=weblogic      hard   nofile   500000)
changed: [bcne-wl-pf04] => (item=weblogic      hard   nofile   500000)
changed: [bcne-wl-pf02] => (item=weblogic      hard   nofile   500000)
changed: [bcne-wl-pf05] => (item=weblogic      hard   nofile   500000)
changed: [bcne-wl-pf01] => (item=weblogic      soft   nofile   500000)
changed: [bcne-wl-pf02] => (item=weblogic      soft   nofile   500000)
changed: [bcne-wl-pf03] => (item=weblogic      soft   nofile   500000)
changed: [bcne-wl-pf04] => (item=weblogic      soft   nofile   500000)
changed: [bcne-wl-pf05] => (item=weblogic      soft   nofile   500000)
changed: [bcne-wl-pf01] => (item=weblogic      hard   nproc    500000)
changed: [bcne-wl-pf03] => (item=weblogic      hard   nproc    500000)
changed: [bcne-wl-pf02] => (item=weblogic      hard   nproc    500000)
changed: [bcne-wl-pf05] => (item=weblogic      hard   nproc    500000)
changed: [bcne-wl-pf04] => (item=weblogic      hard   nproc    500000)
changed: [bcne-wl-pf03] => (item=weblogic      soft   nproc    500000)
changed: [bcne-wl-pf01] => (item=weblogic      soft   nproc    500000)
changed: [bcne-wl-pf02] => (item=weblogic      soft   nproc    500000)
changed: [bcne-wl-pf05] => (item=weblogic      soft   nproc    500000)
changed: [bcne-wl-pf04] => (item=weblogic      soft   nproc    500000)
changed: [bcne-wl-pf06] => (item=weblogic      hard   nofile   500000)
changed: [bcne-con-pf01] => (item=cprime      hard   nofile   500000)
changed: [bcne-lb-pf01] => (item=httpd      hard   nofile   500000)
changed: [bcne-con-pf02] => (item=cprime      hard   nofile   500000)
changed: [bcne-wl-pf06] => (item=weblogic      soft   nofile   500000)
changed: [bcne-con-pf01] => (item=cprime      soft   nofile   500000)
changed: [bcne-con-pf02] => (item=cprime      soft   nofile   500000)
changed: [bcne-lb-pf01] => (item=httpd      soft   nofile   500000)
changed: [bcne-wl-pf06] => (item=weblogic      hard   nproc    500000)
changed: [bcne-lb-pf01] => (item=httpd      hard   nproc    500000)
changed: [bcne-con-pf02] => (item=cprime      hard   nproc    500000)
changed: [bcne-con-pf01] => (item=cprime      hard   nproc    500000)
changed: [bcne-wl-pf06] => (item=weblogic      soft   nproc    500000)
changed: [bcne-con-pf01] => (item=cprime      soft   nproc    500000)
changed: [bcne-con-pf02] => (item=cprime      soft   nproc    500000)
changed: [bcne-lb-pf01] => (item=httpd      soft   nproc    500000)

TASK [he-limits-conf : add limits.conf from group list] ************************
Friday 03 May 2019  13:21:20 -0400 (0:00:03.855)       0:04:20.189 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-limits-conf : Update sysctl settings from all list] *******************
Friday 03 May 2019  13:21:20 -0400 (0:00:00.270)       0:04:20.460 ************ 
changed: [bcne-wl-pf03] => (item={u'name': u'fs.file-max', u'value': 6815744})
changed: [bcne-wl-pf04] => (item={u'name': u'fs.file-max', u'value': 6815744})
changed: [bcne-wl-pf05] => (item={u'name': u'fs.file-max', u'value': 6815744})
changed: [bcne-wl-pf01] => (item={u'name': u'fs.file-max', u'value': 6815744})
changed: [bcne-wl-pf02] => (item={u'name': u'fs.file-max', u'value': 6815744})
changed: [bcne-wl-pf06] => (item={u'name': u'fs.file-max', u'value': 6815744})
changed: [bcne-con-pf01] => (item={u'name': u'fs.file-max', u'value': 6815744})
changed: [bcne-con-pf02] => (item={u'name': u'fs.file-max', u'value': 6815744})
changed: [bcne-lb-pf01] => (item={u'name': u'fs.file-max', u'value': 6815744})

TASK [he-limits-conf : Update sysctl settings from group list] *****************
Friday 03 May 2019  13:21:21 -0400 (0:00:01.291)       0:04:21.752 ************ 

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:21 -0400 (0:00:00.215)       0:04:21.967 ************ 
ok: [bcne-wl-pf01] => {
    "msg": "Tunes applicable to all servers may be placed here with NO when clause."
}
ok: [bcne-wl-pf02] => {
    "msg": "Tunes applicable to all servers may be placed here with NO when clause."
}
ok: [bcne-wl-pf03] => {
    "msg": "Tunes applicable to all servers may be placed here with NO when clause."
}
ok: [bcne-wl-pf05] => {
    "msg": "Tunes applicable to all servers may be placed here with NO when clause."
}
ok: [bcne-wl-pf04] => {
    "msg": "Tunes applicable to all servers may be placed here with NO when clause."
}
ok: [bcne-wl-pf06] => {
    "msg": "Tunes applicable to all servers may be placed here with NO when clause."
}
ok: [bcne-con-pf02] => {
    "msg": "Tunes applicable to all servers may be placed here with NO when clause."
}
ok: [bcne-con-pf01] => {
    "msg": "Tunes applicable to all servers may be placed here with NO when clause."
}
ok: [bcne-lb-pf01] => {
    "msg": "Tunes applicable to all servers may be placed here with NO when clause."
}

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:22 -0400 (0:00:00.384)       0:04:22.351 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:22 -0400 (0:00:00.185)       0:04:22.537 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:22 -0400 (0:00:00.228)       0:04:22.765 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:22 -0400 (0:00:00.233)       0:04:22.999 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:23 -0400 (0:00:00.217)       0:04:23.217 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-lb-pf01]
ok: [bcne-con-pf01] => {
    "msg": "Tunes applicable to group connector may be placed here with following where clause."
}
ok: [bcne-con-pf02] => {
    "msg": "Tunes applicable to group connector may be placed here with following where clause."
}

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:23 -0400 (0:00:00.391)       0:04:23.608 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-lb-pf01]
skipping: [bcne-con-pf02]

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:23 -0400 (0:00:00.295)       0:04:23.903 ************ 
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-wl-pf04]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:24 -0400 (0:00:00.354)       0:04:24.258 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:24 -0400 (0:00:00.293)       0:04:24.551 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-con-pf01]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:24 -0400 (0:00:00.243)       0:04:24.795 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-os-tuning : debug] ****************************************************
Friday 03 May 2019  13:21:25 -0400 (0:00:00.237)       0:04:25.032 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
skipping: [bcne-con-pf01]
skipping: [bcne-con-pf02]
skipping: [bcne-lb-pf01]

TASK [he-ops-utils : create tmp directory on target] ***************************
Friday 03 May 2019  13:21:25 -0400 (0:00:00.317)       0:04:25.349 ************ 
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf05]
changed: [bcne-con-pf01]
changed: [bcne-wl-pf06]
changed: [bcne-con-pf02]
changed: [bcne-lb-pf01]

TASK [he-ops-utils : Unarchive ops-util distrib] *******************************
Friday 03 May 2019  13:21:26 -0400 (0:00:01.038)       0:04:26.388 ************ 
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf06]
changed: [bcne-con-pf02]
changed: [bcne-con-pf01]
changed: [bcne-lb-pf01]

TASK [he-ops-utils : sync CMDB distrib on target] ******************************
Friday 03 May 2019  13:21:32 -0400 (0:00:06.396)       0:04:32.784 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf06]
changed: [bcne-con-pf01]
changed: [bcne-con-pf02]
changed: [bcne-lb-pf01]

TASK [he-ops-utils : create heTools dir and set perms/ownership] ***************
Friday 03 May 2019  13:21:33 -0400 (0:00:01.069)       0:04:33.854 ************ 
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf05]
changed: [bcne-con-pf01]
changed: [bcne-con-pf02]
changed: [bcne-wl-pf06]
changed: [bcne-lb-pf01]

TASK [he-ops-utils : sync heTools items to target] *****************************
Friday 03 May 2019  13:21:34 -0400 (0:00:01.144)       0:04:34.998 ************ 
changed: [bcne-wl-pf01] => (item=fileManager)
changed: [bcne-wl-pf02] => (item=fileManager)
changed: [bcne-wl-pf03] => (item=fileManager)
changed: [bcne-wl-pf04] => (item=fileManager)
changed: [bcne-wl-pf05] => (item=fileManager)
changed: [bcne-wl-pf01] => (item=sirDumpsALot)
changed: [bcne-wl-pf02] => (item=sirDumpsALot)
changed: [bcne-wl-pf04] => (item=sirDumpsALot)
changed: [bcne-wl-pf03] => (item=sirDumpsALot)
changed: [bcne-wl-pf01] => (item=autoBatch)
changed: [bcne-wl-pf05] => (item=sirDumpsALot)
changed: [bcne-wl-pf02] => (item=melissaScripts)
changed: [bcne-wl-pf01] => (item=missingBootstraptacular)
changed: [bcne-wl-pf04] => (item=melissaScripts)
changed: [bcne-wl-pf05] => (item=melissaScripts)
changed: [bcne-wl-pf03] => (item=melissaScripts)
changed: [bcne-wl-pf01] => (item=blackout)
changed: [bcne-con-pf01] => (item=sirDumpsALot)
changed: [bcne-wl-pf06] => (item=fileManager)
changed: [bcne-con-pf02] => (item=sirDumpsALot)
changed: [bcne-lb-pf01] => (item=fileManager)
changed: [bcne-wl-pf01] => (item=melissaScripts)
changed: [bcne-con-pf01] => (item=blackout)
changed: [bcne-wl-pf06] => (item=sirDumpsALot)
changed: [bcne-con-pf02] => (item=blackout)
changed: [bcne-wl-pf06] => (item=melissaScripts)

TASK [he-ops-utils : Add hosts to targets servers.txt file] ********************
Friday 03 May 2019  13:21:38 -0400 (0:00:03.505)       0:04:38.503 ************ 
changed: [bcne-wl-pf01] => (item=bcne-wl-pf01)
changed: [bcne-wl-pf04] => (item=bcne-wl-pf01)
changed: [bcne-wl-pf05] => (item=bcne-wl-pf01)
changed: [bcne-wl-pf03] => (item=bcne-wl-pf01)
changed: [bcne-wl-pf02] => (item=bcne-wl-pf01)
changed: [bcne-wl-pf01] => (item=bcne-wl-pf02)
changed: [bcne-wl-pf04] => (item=bcne-wl-pf02)
changed: [bcne-wl-pf02] => (item=bcne-wl-pf02)
changed: [bcne-wl-pf05] => (item=bcne-wl-pf02)
changed: [bcne-wl-pf03] => (item=bcne-wl-pf02)
changed: [bcne-wl-pf01] => (item=bcne-wl-pf03)
changed: [bcne-wl-pf05] => (item=bcne-wl-pf03)
changed: [bcne-wl-pf02] => (item=bcne-wl-pf03)
changed: [bcne-wl-pf03] => (item=bcne-wl-pf03)
changed: [bcne-wl-pf01] => (item=bcne-wl-pf04)
changed: [bcne-wl-pf04] => (item=bcne-wl-pf03)
changed: [bcne-wl-pf04] => (item=bcne-wl-pf04)
changed: [bcne-wl-pf05] => (item=bcne-wl-pf04)
changed: [bcne-wl-pf03] => (item=bcne-wl-pf04)
changed: [bcne-wl-pf01] => (item=bcne-wl-pf05)
changed: [bcne-wl-pf02] => (item=bcne-wl-pf04)
changed: [bcne-wl-pf02] => (item=bcne-wl-pf05)
changed: [bcne-wl-pf05] => (item=bcne-wl-pf05)
changed: [bcne-wl-pf04] => (item=bcne-wl-pf05)
changed: [bcne-wl-pf03] => (item=bcne-wl-pf05)
changed: [bcne-wl-pf01] => (item=bcne-wl-pf06)
changed: [bcne-wl-pf04] => (item=bcne-wl-pf06)
changed: [bcne-wl-pf01] => (item=bcne-con-pf01)
changed: [bcne-wl-pf02] => (item=bcne-wl-pf06)
changed: [bcne-wl-pf03] => (item=bcne-wl-pf06)
changed: [bcne-wl-pf05] => (item=bcne-wl-pf06)
changed: [bcne-wl-pf04] => (item=bcne-con-pf01)
changed: [bcne-wl-pf02] => (item=bcne-con-pf01)
changed: [bcne-wl-pf03] => (item=bcne-con-pf01)
changed: [bcne-wl-pf01] => (item=bcne-con-pf02)
changed: [bcne-wl-pf05] => (item=bcne-con-pf01)
changed: [bcne-wl-pf04] => (item=bcne-con-pf02)
changed: [bcne-wl-pf01] => (item=bcne-lb-pf01)
changed: [bcne-wl-pf02] => (item=bcne-con-pf02)
changed: [bcne-wl-pf05] => (item=bcne-con-pf02)
changed: [bcne-wl-pf03] => (item=bcne-con-pf02)
changed: [bcne-wl-pf04] => (item=bcne-lb-pf01)
changed: [bcne-wl-pf05] => (item=bcne-lb-pf01)
changed: [bcne-wl-pf02] => (item=bcne-lb-pf01)
changed: [bcne-wl-pf03] => (item=bcne-lb-pf01)
changed: [bcne-wl-pf06] => (item=bcne-wl-pf01)
changed: [bcne-con-pf01] => (item=bcne-wl-pf01)
changed: [bcne-con-pf02] => (item=bcne-wl-pf01)
changed: [bcne-wl-pf06] => (item=bcne-wl-pf02)
changed: [bcne-con-pf01] => (item=bcne-wl-pf02)
changed: [bcne-lb-pf01] => (item=bcne-wl-pf01)
changed: [bcne-con-pf02] => (item=bcne-wl-pf02)
changed: [bcne-wl-pf06] => (item=bcne-wl-pf03)
changed: [bcne-con-pf01] => (item=bcne-wl-pf03)
changed: [bcne-lb-pf01] => (item=bcne-wl-pf02)
changed: [bcne-con-pf02] => (item=bcne-wl-pf03)
changed: [bcne-con-pf01] => (item=bcne-wl-pf04)
changed: [bcne-lb-pf01] => (item=bcne-wl-pf03)
changed: [bcne-wl-pf06] => (item=bcne-wl-pf04)
changed: [bcne-con-pf02] => (item=bcne-wl-pf04)
changed: [bcne-con-pf01] => (item=bcne-wl-pf05)
changed: [bcne-lb-pf01] => (item=bcne-wl-pf04)
changed: [bcne-wl-pf06] => (item=bcne-wl-pf05)
changed: [bcne-con-pf02] => (item=bcne-wl-pf05)
changed: [bcne-con-pf01] => (item=bcne-wl-pf06)
changed: [bcne-wl-pf06] => (item=bcne-wl-pf06)
changed: [bcne-con-pf02] => (item=bcne-wl-pf06)
changed: [bcne-lb-pf01] => (item=bcne-wl-pf05)
changed: [bcne-con-pf01] => (item=bcne-con-pf01)
changed: [bcne-lb-pf01] => (item=bcne-wl-pf06)
changed: [bcne-wl-pf06] => (item=bcne-con-pf01)
changed: [bcne-con-pf02] => (item=bcne-con-pf01)
changed: [bcne-con-pf01] => (item=bcne-con-pf02)
changed: [bcne-wl-pf06] => (item=bcne-con-pf02)
changed: [bcne-con-pf02] => (item=bcne-con-pf02)
changed: [bcne-lb-pf01] => (item=bcne-con-pf01)
changed: [bcne-con-pf01] => (item=bcne-lb-pf01)
changed: [bcne-wl-pf06] => (item=bcne-lb-pf01)
changed: [bcne-lb-pf01] => (item=bcne-con-pf02)
changed: [bcne-con-pf02] => (item=bcne-lb-pf01)
changed: [bcne-lb-pf01] => (item=bcne-lb-pf01)

TASK [he-ops-utils : debug] ****************************************************
Friday 03 May 2019  13:21:51 -0400 (0:00:12.649)       0:04:51.153 ************ 
ok: [bcne-wl-pf01] => {
    "hosts_changed.changed": true
}
ok: [bcne-wl-pf05] => {
    "hosts_changed.changed": true
}
ok: [bcne-wl-pf03] => {
    "hosts_changed.changed": true
}
ok: [bcne-wl-pf02] => {
    "hosts_changed.changed": true
}
ok: [bcne-wl-pf04] => {
    "hosts_changed.changed": true
}
ok: [bcne-wl-pf06] => {
    "hosts_changed.changed": true
}
ok: [bcne-con-pf01] => {
    "hosts_changed.changed": true
}
ok: [bcne-lb-pf01] => {
    "hosts_changed.changed": true
}
ok: [bcne-con-pf02] => {
    "hosts_changed.changed": true
}

TASK [he-ops-utils : sync CMDB envProperties meta-data to target] **************
Friday 03 May 2019  13:21:52 -0400 (0:00:00.868)       0:04:52.022 ************ 
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf04]
changed: [bcne-con-pf02]
changed: [bcne-wl-pf06]
changed: [bcne-lb-pf01]
changed: [bcne-con-pf01]

TASK [he-ops-utils : chmod/chown] **********************************************
Friday 03 May 2019  13:22:00 -0400 (0:00:08.497)       0:05:00.520 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf06]
changed: [bcne-con-pf01]
changed: [bcne-con-pf02]
changed: [bcne-lb-pf01]

TASK [he-ops-utils : create .ssh directory on secondaries] *********************
Friday 03 May 2019  13:22:02 -0400 (0:00:02.227)       0:05:02.748 ************ 
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]
ok: [bcne-con-pf02]
ok: [bcne-con-pf01]
ok: [bcne-lb-pf01]

TASK [he-ops-utils : Add 'normal' crontab entries] *****************************
Friday 03 May 2019  13:22:04 -0400 (0:00:01.458)       0:05:04.207 ************ 
changed: [bcne-wl-pf03] => (item={'value': {u'hour': u'01', u'month': u'*', u'disabled': False, u'job': u'/home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log', u'weekday': u'*', u'backup': u'yes', u'day': u'*', u'minute': u'59'}, 'key': u'fileManager'})
changed: [bcne-wl-pf02] => (item={'value': {u'hour': u'01', u'month': u'*', u'disabled': False, u'job': u'/home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log', u'weekday': u'*', u'backup': u'yes', u'day': u'*', u'minute': u'59'}, 'key': u'fileManager'})
changed: [bcne-wl-pf01] => (item={'value': {u'hour': u'01', u'month': u'*', u'disabled': False, u'job': u'/home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log', u'weekday': u'*', u'backup': u'yes', u'day': u'*', u'minute': u'59'}, 'key': u'fileManager'})
changed: [bcne-wl-pf05] => (item={'value': {u'hour': u'01', u'month': u'*', u'disabled': False, u'job': u'/home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log', u'weekday': u'*', u'backup': u'yes', u'day': u'*', u'minute': u'59'}, 'key': u'fileManager'})
changed: [bcne-wl-pf04] => (item={'value': {u'hour': u'01', u'month': u'*', u'disabled': False, u'job': u'/home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log', u'weekday': u'*', u'backup': u'yes', u'day': u'*', u'minute': u'59'}, 'key': u'fileManager'})
changed: [bcne-wl-pf01] => (item={'value': {u'hour': u'*', u'month': u'*', u'disabled': True, u'job': u'/home/weblogic/heTools/autoBatch/scripts/autoBatch.sh -execute &> /tmp/out.autoBatch.cron.log', u'weekday': u'*', u'day': u'*', u'minute': u'*/1'}, 'key': u'autoBatch'})
changed: [bcne-wl-pf06] => (item={'value': {u'hour': u'01', u'month': u'*', u'disabled': False, u'job': u'/home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log', u'weekday': u'*', u'backup': u'yes', u'day': u'*', u'minute': u'59'}, 'key': u'fileManager'})
changed: [bcne-lb-pf01] => (item={'value': {u'hour': u'01', u'month': u'*', u'disabled': False, u'job': u'/home/httpd/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log', u'weekday': u'*', u'backup': u'yes', u'day': u'*', u'minute': u'59'}, 'key': u'fileManager'})
changed: [bcne-wl-pf01] => (item={'value': {u'hour': u'01', u'month': u'*', u'disabled': False, u'job': u'/home/weblogic/heTools/missingBootstraptacular/scripts/missingBootstraptacular.sh performance -execute &> /tmp/out.missingBootstraptacular.log', u'weekday': u'*', u'day': u'*', u'minute': u'30'}, 'key': u'missingBootStrapTactular'})

TASK [he-ops-utils : diff crontab changes] *************************************
Friday 03 May 2019  13:22:06 -0400 (0:00:01.812)       0:05:06.019 ************ 
ok: [bcne-wl-pf01] => (item=/tmp/crontabmTUSGq)
ok: [bcne-wl-pf03] => (item=/tmp/crontabKqyAQz)
ok: [bcne-wl-pf02] => (item=/tmp/crontabh90JZQ)
ok: [bcne-wl-pf04] => (item=/tmp/crontabZn0ZZL)
ok: [bcne-wl-pf05] => (item=/tmp/crontabe6YRG9)
ok: [bcne-wl-pf06] => (item=/tmp/crontabYZxjkF)
ok: [bcne-lb-pf01] => (item=/tmp/crontab4vI26K)

TASK [he-ops-utils : display crontab diff] *************************************
Friday 03 May 2019  13:22:07 -0400 (0:00:01.296)       0:05:07.315 ************ 
ok: [bcne-wl-pf01] => {
    "cron_diff.results|map(attribute='stdout_lines')|list": [
        [
            "0a1,6", 
            "> #Ansible: fileManager", 
            "> 59 01 * * * /home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log", 
            "> #Ansible: autoBatch", 
            "> #*/1 * * * * /home/weblogic/heTools/autoBatch/scripts/autoBatch.sh -execute &> /tmp/out.autoBatch.cron.log", 
            "> #Ansible: missingBootStrapTactular", 
            "> 30 01 * * * /home/weblogic/heTools/missingBootstraptacular/scripts/missingBootstraptacular.sh performance -execute &> /tmp/out.missingBootstraptacular.log"
        ]
    ]
}
ok: [bcne-wl-pf03] => {
    "cron_diff.results|map(attribute='stdout_lines')|list": [
        [
            "0a1,2", 
            "> #Ansible: fileManager", 
            "> 59 01 * * * /home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log"
        ]
    ]
}
ok: [bcne-wl-pf02] => {
    "cron_diff.results|map(attribute='stdout_lines')|list": [
        [
            "0a1,2", 
            "> #Ansible: fileManager", 
            "> 59 01 * * * /home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log"
        ]
    ]
}
ok: [bcne-wl-pf05] => {
    "cron_diff.results|map(attribute='stdout_lines')|list": [
        [
            "0a1,2", 
            "> #Ansible: fileManager", 
            "> 59 01 * * * /home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log"
        ]
    ]
}
skipping: [bcne-con-pf01]
ok: [bcne-wl-pf04] => {
    "cron_diff.results|map(attribute='stdout_lines')|list": [
        [
            "0a1,2", 
            "> #Ansible: fileManager", 
            "> 59 01 * * * /home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log"
        ]
    ]
}
skipping: [bcne-con-pf02]
ok: [bcne-wl-pf06] => {
    "cron_diff.results|map(attribute='stdout_lines')|list": [
        [
            "0a1,2", 
            "> #Ansible: fileManager", 
            "> 59 01 * * * /home/weblogic/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log"
        ]
    ]
}
ok: [bcne-lb-pf01] => {
    "cron_diff.results|map(attribute='stdout_lines')|list": [
        [
            "0a1,2", 
            "> #Ansible: fileManager", 
            "> 59 01 * * * /home/httpd/heTools/fileManager/scripts/fileManager.sh -execute &> /tmp/out.fileManager.cron.log"
        ]
    ]
}

TASK [he-ops-utils : Add 'special' crontab entries] ****************************
Friday 03 May 2019  13:22:08 -0400 (0:00:00.727)       0:05:08.043 ************ 
changed: [bcne-wl-pf01] => (item={'value': {u'disabled': True, u'job': u'/home/weblogic/heTools/sirDumpsALot/sirDumpsALot.sh claimserver 10 &> /tmp/out.sirDumpsALot.cron.log', u'special_time': u'reboot'}, 'key': u'sirDumpsALot_cs'})
changed: [bcne-con-pf01] => (item={'value': {u'disabled': True, u'job': u'/home/cprime/heTools/sirDumpsALot/sirDumpsALot.sh Connector     10 FROMFILE &> /tmp/out.sirDumpsALot.cron.log', u'special_time': u'reboot'}, 'key': u'sirDumpsALot'})
changed: [bcne-con-pf02] => (item={'value': {u'disabled': True, u'job': u'/home/cprime/heTools/sirDumpsALot/sirDumpsALot.sh Connector     10 FROMFILE &> /tmp/out.sirDumpsALot.cron.log', u'special_time': u'reboot'}, 'key': u'sirDumpsALot'})
changed: [bcne-wl-pf01] => (item={'value': {u'disabled': True, u'job': u'/home/weblogic/heTools/sirDumpsALot/sirDumpsALot.sh externaljms 10 &> /tmp/out.sirDumpsALot.cron.log', u'special_time': u'reboot'}, 'key': u'sirDumpsALot_ej'})

TASK [he-os-sudoers : Copy application admin sudoers file to target] ***********
Friday 03 May 2019  13:22:08 -0400 (0:00:00.949)       0:05:08.992 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf04]
changed: [bcne-con-pf01]
changed: [bcne-wl-pf06]
changed: [bcne-con-pf02]
changed: [bcne-lb-pf01]

TASK [he-os-sudoers : Copy IT DBA sudoers file to target] **********************
Friday 03 May 2019  13:22:11 -0400 (0:00:02.196)       0:05:11.189 ************ 
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf06]
changed: [bcne-con-pf01]
changed: [bcne-lb-pf01]
changed: [bcne-con-pf02]

PLAY [Exchange keys with Payor servers] ****************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:22:13 -0400 (0:00:02.234)       0:05:13.424 ************ 
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]

TASK [he-payor-secondary-key-xchange : get public key from primary payor server] ***
Friday 03 May 2019  13:22:14 -0400 (0:00:01.518)       0:05:14.942 ************ 
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]
changed: [bcne-wl-pf01]

TASK [he-payor-secondary-key-xchange : set_fact] *******************************
Friday 03 May 2019  13:22:15 -0400 (0:00:00.368)       0:05:15.311 ************ 
ok: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]

TASK [he-payor-secondary-key-xchange : create .ssh directory on secondaries] ***
Friday 03 May 2019  13:22:15 -0400 (0:00:00.127)       0:05:15.439 ************ 
skipping: [bcne-wl-pf01]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf06]
ok: [bcne-wl-pf05]

TASK [he-payor-secondary-key-xchange : create authorized_keys file on secondaries] ***
Friday 03 May 2019  13:22:16 -0400 (0:00:00.608)       0:05:16.047 ************ 
skipping: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf06]
changed: [bcne-wl-pf05]

TASK [he-payor-secondary-key-xchange : Insert public key from primary into secondaries authorized keys.] ***
Friday 03 May 2019  13:22:16 -0400 (0:00:00.837)       0:05:16.885 ************ 
skipping: [bcne-wl-pf01]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf06]

PLAY [Set ssh config on Payor primary] *****************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:22:17 -0400 (0:00:01.008)       0:05:17.893 ************ 
ok: [bcne-wl-pf01]

TASK [he-payor-primary-ssh-config : create .ssh directory on primary] **********
Friday 03 May 2019  13:22:18 -0400 (0:00:00.466)       0:05:18.360 ************ 
ok: [bcne-wl-pf01]

TASK [he-payor-primary-ssh-config : Create ssh config to ignore hostkey checks] ***
Friday 03 May 2019  13:22:18 -0400 (0:00:00.308)       0:05:18.668 ************ 
changed: [bcne-wl-pf01]
 [WARNING]: Could not match supplied host pattern, ignoring: caremanager
 [WARNING]: Could not match supplied host pattern, ignoring: caremanager-
integration

PLAY [Install libaio for SQLPLUS] **********************************************
skipping: no hosts matched

PLAY [Install load balancer] ***************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:22:19 -0400 (0:00:00.631)       0:05:19.300 ************ 
ok: [bcne-lb-pf01]

TASK [he-payor-apachelb-install : yum install httpd24u and httpd24u-mod_ssl from IUS repo] ***
Friday 03 May 2019  13:22:19 -0400 (0:00:00.483)       0:05:19.784 ************ 
changed: [bcne-lb-pf01]

TASK [he-payor-apachelb-install : sync lb.base to /home/httpd] *****************
Friday 03 May 2019  13:22:29 -0400 (0:00:09.463)       0:05:29.247 ************ 
changed: [bcne-lb-pf01]

TASK [he-payor-apachelb-install : Create lb logs directory] ********************
Friday 03 May 2019  13:22:30 -0400 (0:00:01.070)       0:05:30.318 ************ 
changed: [bcne-lb-pf01]

TASK [he-payor-apachelb-install : sync lb.instances] ***************************
Friday 03 May 2019  13:22:30 -0400 (0:00:00.325)       0:05:30.644 ************ 
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5555'}, 'key': u'ui'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_WLProxySSL': u'off', u'payor_apachelb_port': u'5557', u'payor_apachelb_WLProxySSLPassThrough': u'off', u'payor_apachelb_SSLEngine': u'off'}, 'key': u'integration'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5559'}, 'key': u'axiom'})

TASK [he-payor-apachelb-install : Create httpd symlink for lb.instances] *******
Friday 03 May 2019  13:22:33 -0400 (0:00:02.726)       0:05:33.370 ************ 
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5555'}, 'key': u'ui'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_WLProxySSL': u'off', u'payor_apachelb_port': u'5557', u'payor_apachelb_WLProxySSLPassThrough': u'off', u'payor_apachelb_SSLEngine': u'off'}, 'key': u'integration'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5559'}, 'key': u'axiom'})

TASK [he-payor-apachelb-install : Create logs symlink for lb.instances] ********
Friday 03 May 2019  13:22:34 -0400 (0:00:00.942)       0:05:34.312 ************ 
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5555'}, 'key': u'ui'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_WLProxySSL': u'off', u'payor_apachelb_port': u'5557', u'payor_apachelb_WLProxySSLPassThrough': u'off', u'payor_apachelb_SSLEngine': u'off'}, 'key': u'integration'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5559'}, 'key': u'axiom'})

TASK [he-payor-apachelb-install : Create modules symlink for lb.instances] *****
Friday 03 May 2019  13:22:35 -0400 (0:00:00.883)       0:05:35.196 ************ 
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5555'}, 'key': u'ui'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_WLProxySSL': u'off', u'payor_apachelb_port': u'5557', u'payor_apachelb_WLProxySSLPassThrough': u'off', u'payor_apachelb_SSLEngine': u'off'}, 'key': u'integration'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5559'}, 'key': u'axiom'})

TASK [he-payor-apachelb-install : Create ssl symlink for lb.instances] *********
Friday 03 May 2019  13:22:36 -0400 (0:00:00.909)       0:05:36.105 ************ 
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5555'}, 'key': u'ui'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_WLProxySSL': u'off', u'payor_apachelb_port': u'5557', u'payor_apachelb_WLProxySSLPassThrough': u'off', u'payor_apachelb_SSLEngine': u'off'}, 'key': u'integration'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5559'}, 'key': u'axiom'})

TASK [he-payor-apachelb-install : place httpd.confs from template] *************
Friday 03 May 2019  13:22:37 -0400 (0:00:00.911)       0:05:37.017 ************ 
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5555'}, 'key': u'ui'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_WLProxySSL': u'off', u'payor_apachelb_port': u'5557', u'payor_apachelb_WLProxySSLPassThrough': u'off', u'payor_apachelb_SSLEngine': u'off'}, 'key': u'integration'})
changed: [bcne-lb-pf01] => (item={'value': {u'payor_apachelb_port': u'5559'}, 'key': u'axiom'})

TASK [he-payor-apachelb-install : Change permissions on the lb directory] ******
Friday 03 May 2019  13:22:39 -0400 (0:00:02.063)       0:05:39.081 ************ 
changed: [bcne-lb-pf01]

TASK [he-payor-apachelb-install : create local temp dir for extracting WLS plugin] ***
Friday 03 May 2019  13:22:39 -0400 (0:00:00.347)       0:05:39.428 ************ 
ok: [bcne-lb-pf01 -> localhost]

TASK [he-payor-apachelb-install : set wls_temp_dir] ****************************
Friday 03 May 2019  13:22:39 -0400 (0:00:00.457)       0:05:39.885 ************ 
ok: [bcne-lb-pf01]

TASK [he-payor-apachelb-install : Unarchive WLS plugin distribution into temp location] ***
Friday 03 May 2019  13:22:39 -0400 (0:00:00.060)       0:05:39.946 ************ 
ok: [bcne-lb-pf01 -> localhost]

TASK [he-payor-apachelb-install : install WLS plugin modules] ******************
Friday 03 May 2019  13:22:44 -0400 (0:00:04.057)       0:05:44.004 ************ 
changed: [bcne-lb-pf01]

TASK [he-payor-apachelb-install : Start all lb servers] ************************
Friday 03 May 2019  13:22:59 -0400 (0:00:15.515)       0:05:59.519 ************ 
changed: [bcne-lb-pf01]

TASK [he-payor-apachelb-install : Get status of lb servers] ********************
Friday 03 May 2019  13:23:00 -0400 (0:00:00.655)       0:06:00.175 ************ 
ok: [bcne-lb-pf01]

TASK [he-payor-apachelb-install : debug] ***************************************
Friday 03 May 2019  13:23:00 -0400 (0:00:00.580)       0:06:00.755 ************ 
ok: [bcne-lb-pf01] => {
    "status_results": {
        "changed": false, 
        "cmd": "source /home/httpd/.bashrc;./status", 
        "delta": "0:00:00.205610", 
        "end": "2019-05-03 12:23:03.568029", 
        "failed": false, 
        "rc": 0, 
        "start": "2019-05-03 12:23:03.362419", 
        "stderr": "", 
        "stderr_lines": [], 
        "stdout": "#################################################################################\n## Load Balancer Status (v1.0)\n##\n## LBHOME:                  /home/httpd/lb\n## LB Instances Detected:   3\n##\n\nName      IP                 Port    SSLIn  SSLOut  WLSServers           Running\n--------  ---------------    -----   -----  ------  -----------------    --------\naxiom     172.29.15.198      5559    on     off     \"bcne-wl-pf05:3081   Yes     \n                                                    bcne-wl-pf06:3101\"\n\nintegration172.29.15.198      5557    off    off     \"bcne-wl-pf05:3071   Yes     \n                                                    bcne-wl-pf06:3091\"\n\nui        172.29.15.198      5555    on     off     \"bcne-wl-pf01:3011   Yes     \n                                                    bcne-wl-pf02:3021\n                                                    bcne-wl-pf02:3031\n                                                    bcne-wl-pf02:3041\"", 
        "stdout_lines": [
            "#################################################################################", 
            "## Load Balancer Status (v1.0)", 
            "##", 
            "## LBHOME:                  /home/httpd/lb", 
            "## LB Instances Detected:   3", 
            "##", 
            "", 
            "Name      IP                 Port    SSLIn  SSLOut  WLSServers           Running", 
            "--------  ---------------    -----   -----  ------  -----------------    --------", 
            "axiom     172.29.15.198      5559    on     off     \"bcne-wl-pf05:3081   Yes     ", 
            "                                                    bcne-wl-pf06:3101\"", 
            "", 
            "integration172.29.15.198      5557    off    off     \"bcne-wl-pf05:3071   Yes     ", 
            "                                                    bcne-wl-pf06:3091\"", 
            "", 
            "ui        172.29.15.198      5555    on     off     \"bcne-wl-pf01:3011   Yes     ", 
            "                                                    bcne-wl-pf02:3021", 
            "                                                    bcne-wl-pf02:3031", 
            "                                                    bcne-wl-pf02:3041\""
        ]
    }
}

TASK [he-payor-apachelb-install : Get running statuses] ************************
Friday 03 May 2019  13:23:00 -0400 (0:00:00.076)       0:06:00.832 ************ 
ok: [bcne-lb-pf01]

TASK [he-payor-apachelb-install : Fail if all instances are not running] *******
Friday 03 May 2019  13:23:01 -0400 (0:00:00.524)       0:06:01.356 ************ 
skipping: [bcne-lb-pf01] => (item=Yes) 
skipping: [bcne-lb-pf01] => (item=Yes) 
skipping: [bcne-lb-pf01] => (item=Yes) 

TASK [he-payor-apachelb-install : Restart LoadBalancer at reboot] **************
Friday 03 May 2019  13:23:01 -0400 (0:00:00.049)       0:06:01.406 ************ 
changed: [bcne-lb-pf01]
 [WARNING]: Could not match supplied host pattern, ignoring: payor-iway

PLAY [Configure iWay server] ***************************************************
skipping: no hosts matched

PLAY [Upload or mount MelissaData] *********************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  13:23:01 -0400 (0:00:00.348)       0:06:01.755 ************ 
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]

TASK [he.melissadata : Set melissadata user fact for owner] ********************
Friday 03 May 2019  13:23:03 -0400 (0:00:01.259)       0:06:03.014 ************ 
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf06]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf05]

TASK [he.melissadata : Set melissadata user fact for group] ********************
Friday 03 May 2019  13:23:03 -0400 (0:00:00.208)       0:06:03.223 ************ 
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf06]

TASK [he.melissadata : melissadata_mode is required] ***************************
Friday 03 May 2019  13:23:03 -0400 (0:00:00.268)       0:06:03.491 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]

TASK [he.melissadata : Include task list for melissadata mode] *****************
Friday 03 May 2019  13:23:03 -0400 (0:00:00.114)       0:06:03.606 ************ 
included: /home/ansible/hebb/installer/common/roles/he.melissadata/tasks/local.yml for bcne-wl-pf01, bcne-wl-pf02, bcne-wl-pf03, bcne-wl-pf04, bcne-wl-pf05, bcne-wl-pf06

TASK [he.melissadata : Unmount melissadata to fix stale mount point] ***********
Friday 03 May 2019  13:23:03 -0400 (0:00:00.192)       0:06:03.798 ************ 
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf06]

TASK [he.melissadata : Create directory for melissadata] ***********************
Friday 03 May 2019  13:23:04 -0400 (0:00:01.073)       0:06:04.872 ************ 
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf06]

TASK [he.melissadata : Synchronize melissadata address files] ******************
Friday 03 May 2019  13:23:05 -0400 (0:00:00.753)       0:06:05.625 ************ 
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf06]

TASK [he.melissadata : Synchronize melissadata geocoder files] *****************
Friday 03 May 2019  14:03:00 -0400 (0:39:54.602)       0:46:00.227 ************ 
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf06]

TASK [he.melissadata : Fix ownership for melissadata directory] ****************
Friday 03 May 2019  14:08:45 -0400 (0:05:45.019)       0:51:45.246 ************ 
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf06]

PLAY [Upload 3M GPS] ***********************************************************

TASK [Gathering Facts] *********************************************************
Friday 03 May 2019  14:08:47 -0400 (0:00:01.993)       0:51:47.239 ************ 
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]

TASK [he.3mhis : Install packages required by 3M GPS] **************************
Friday 03 May 2019  14:08:49 -0400 (0:00:01.823)       0:51:49.063 ************ 
changed: [bcne-wl-pf02]
changed: [bcne-wl-pf01]
changed: [bcne-wl-pf05]
changed: [bcne-wl-pf03]
changed: [bcne-wl-pf04]
changed: [bcne-wl-pf06]

TASK [he.3mhis : Check Existing 3M installation] *******************************
Friday 03 May 2019  14:09:42 -0400 (0:00:53.697)       0:52:42.761 ************ 
ok: [bcne-wl-pf04]
ok: [bcne-wl-pf02]
ok: [bcne-wl-pf03]
ok: [bcne-wl-pf01]
ok: [bcne-wl-pf05]
ok: [bcne-wl-pf06]

TASK [he.3mhis : Get version] **************************************************
Friday 03 May 2019  14:09:43 -0400 (0:00:00.860)       0:52:43.621 ************ 
skipping: [bcne-wl-pf01]
skipping: [bcne-wl-pf02]
skipping: [bcne-wl-pf04]
skipping: [bcne-wl-pf03]
skipping: [bcne-wl-pf05]
skipping: [bcne-wl-pf06]

TASK [he.3mhis : Create directories for 3m gps installations] ******************
Friday 03 May 2019  14:09:43 -0400 (0:00:00.237)       0:52:43.859 ************ 
changed: [bcne-wl-pf01] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
skipping: [bcne-wl-pf01] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
changed: [bcne-wl-pf04] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
skipping: [bcne-wl-pf04] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
changed: [bcne-wl-pf05] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
changed: [bcne-wl-pf03] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
changed: [bcne-wl-pf02] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
skipping: [bcne-wl-pf03] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
skipping: [bcne-wl-pf02] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
skipping: [bcne-wl-pf05] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
changed: [bcne-wl-pf01] => (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'})
changed: [bcne-wl-pf04] => (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'})
changed: [bcne-wl-pf03] => (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'})
changed: [bcne-wl-pf02] => (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'})
changed: [bcne-wl-pf05] => (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'})
changed: [bcne-wl-pf06] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
skipping: [bcne-wl-pf06] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
changed: [bcne-wl-pf06] => (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'})

TASK [he.3mhis : Copy mmm distributions to target] *****************************
Friday 03 May 2019  14:09:45 -0400 (0:00:01.452)       0:52:45.311 ************ 
changed: [bcne-wl-pf03] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
skipping: [bcne-wl-pf03] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
An exception occurred during task execution. To see the full traceback, use -vvv. The error was: AnsibleFileNotFound: Could not find or access '/opt/media/gps_201910.tar.gz'
failed: [bcne-wl-pf03] (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'}) => {"changed": false, "item": {"dest": "gps_201910", "file": "gps_201910.tar.gz", "state": "present", "version": "2019.1.0"}, "msg": "Could not find or access '/opt/media/gps_201910.tar.gz'"}
changed: [bcne-wl-pf02] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
skipping: [bcne-wl-pf02] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
An exception occurred during task execution. To see the full traceback, use -vvv. The error was: AnsibleFileNotFound: Could not find or access '/opt/media/gps_201910.tar.gz'
failed: [bcne-wl-pf02] (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'}) => {"changed": false, "item": {"dest": "gps_201910", "file": "gps_201910.tar.gz", "state": "present", "version": "2019.1.0"}, "msg": "Could not find or access '/opt/media/gps_201910.tar.gz'"}
changed: [bcne-wl-pf01] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
changed: [bcne-wl-pf04] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
skipping: [bcne-wl-pf04] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
An exception occurred during task execution. To see the full traceback, use -vvv. The error was: AnsibleFileNotFound: Could not find or access '/opt/media/gps_201910.tar.gz'
failed: [bcne-wl-pf04] (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'}) => {"changed": false, "item": {"dest": "gps_201910", "file": "gps_201910.tar.gz", "state": "present", "version": "2019.1.0"}, "msg": "Could not find or access '/opt/media/gps_201910.tar.gz'"}
skipping: [bcne-wl-pf01] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
An exception occurred during task execution. To see the full traceback, use -vvv. The error was: AnsibleFileNotFound: Could not find or access '/opt/media/gps_201910.tar.gz'
failed: [bcne-wl-pf01] (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'}) => {"changed": false, "item": {"dest": "gps_201910", "file": "gps_201910.tar.gz", "state": "present", "version": "2019.1.0"}, "msg": "Could not find or access '/opt/media/gps_201910.tar.gz'"}
changed: [bcne-wl-pf05] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
skipping: [bcne-wl-pf05] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
An exception occurred during task execution. To see the full traceback, use -vvv. The error was: AnsibleFileNotFound: Could not find or access '/opt/media/gps_201910.tar.gz'
failed: [bcne-wl-pf05] (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'}) => {"changed": false, "item": {"dest": "gps_201910", "file": "gps_201910.tar.gz", "state": "present", "version": "2019.1.0"}, "msg": "Could not find or access '/opt/media/gps_201910.tar.gz'"}
changed: [bcne-wl-pf06] => (item={u'dest': u'gps_201730', u'state': u'present', u'version': u'2017.3.0', u'file': u'gps_201730.tar.gz'})
skipping: [bcne-wl-pf06] => (item={u'dest': u'gps_201900', u'state': u'absent', u'version': u'2019.0.0', u'file': u'gps_201900.tar.gz'}) 
An exception occurred during task execution. To see the full traceback, use -vvv. The error was: AnsibleFileNotFound: Could not find or access '/opt/media/gps_201910.tar.gz'
failed: [bcne-wl-pf06] (item={u'dest': u'gps_201910', u'state': u'present', u'version': u'2019.1.0', u'file': u'gps_201910.tar.gz'}) => {"changed": false, "item": {"dest": "gps_201910", "file": "gps_201910.tar.gz", "state": "present", "version": "2019.1.0"}, "msg": "Could not find or access '/opt/media/gps_201910.tar.gz'"}

NO MORE HOSTS LEFT *************************************************************
	to retry, use: --limit @/home/ansible/.ansible/retry-files/configure-env.retry

PLAY RECAP *********************************************************************
bcne-con-pf01              : ok=110  changed=31   unreachable=0    failed=0   
bcne-con-pf02              : ok=110  changed=31   unreachable=0    failed=0   
bcne-lb-pf01               : ok=132  changed=44   unreachable=0    failed=0   
bcne-wl-pf01               : ok=134  changed=40   unreachable=0    failed=1   
bcne-wl-pf02               : ok=128  changed=39   unreachable=0    failed=1   
bcne-wl-pf03               : ok=128  changed=39   unreachable=0    failed=1   
bcne-wl-pf04               : ok=128  changed=39   unreachable=0    failed=1   
bcne-wl-pf05               : ok=128  changed=39   unreachable=0    failed=1   
bcne-wl-pf06               : ok=128  changed=39   unreachable=0    failed=1   
localhost                  : ok=62   changed=27   unreachable=0    failed=0   

Friday 03 May 2019  14:12:51 -0400 (0:03:05.797)       0:55:51.109 ************ 
=============================================================================== 
he.melissadata : Synchronize melissadata address files --------------- 2394.60s
he.melissadata : Synchronize melissadata geocoder files --------------- 345.02s
he.3mhis : Copy mmm distributions to target --------------------------- 185.80s
he.3mhis : Install packages required by 3M GPS ------------------------- 53.70s
he-yum-installs : install yum packages all ----------------------------- 18.36s
he-payor-apachelb-install : install WLS plugin modules ----------------- 15.52s
he-ops-utils : Add hosts to targets servers.txt file ------------------- 12.65s
he-payor-apachelb-install : yum install httpd24u and httpd24u-mod_ssl from IUS repo --- 9.46s
he-ops-utils : sync CMDB envProperties meta-data to target -------------- 8.50s
Gathering Facts --------------------------------------------------------- 6.65s
he-ops-utils : Unarchive ops-util distrib ------------------------------- 6.40s
he-generate-installer-properties : Manifest-search: get file-list from connector distrib. --- 5.65s
he-firewalld : Open firewalld ports from group list --------------------- 5.19s
he-generate-installer-properties : Generate cm group_vars/all files ----- 5.06s
he-access-conf : add access.conf whitelist all lines -------------------- 4.81s
he-create-sys-app-user : place desired keys in auth keys for user ------- 4.56s
he-load-metadata : Do we have a defaults.yml in the metadata locale ? --- 4.31s
he-generate-installer-properties : Insure connector distribution is present --- 4.30s
he-load-metadata : Do we have a default customer.yml for this customer? --- 4.12s
he-load-metadata : Do we have a override file for this environment? ----- 4.12s
Playbook run took 0 days, 0 hours, 55 minutes, 51 seconds
