#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

PROPERTY_FILE=./bcbsne_env.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

echo ""
echo "********** BCBSNE SH : bcbsne_bobcob_configure.sh start on ${HOST_NAME} **********"

    echo "Start configuring  bcbsne_bobcob job"

        bobcobConfigFile=$HE_DIR/bcbsne-bobcob/resources/config/bcbsne-bobcob-job-config.json
        chmod -R 755 $bobcobConfigFile

        sed -i -e "s|#HE_DIR#|${HE_DIR}|g" $bobcobConfigFile

        extractStartEndTime=$(date +"%Y-%m-%dT%T.%3NEDT")
        echo "extractStartEndTime: ${extractStartEndTime}"

        sed -i -e "s|SSSS-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $bobcobConfigFile
        sed -i -e "s|EEEE-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $bobcobConfigFile


        configStagingDir=$(grep "extractConfigStagingDir" $HE_DIR/etc/com.healthedge.customer.generic.configuration.cfg | sed -e "s/extractConfigStagingDir=//g")
        echo "configStagingDir: ${configStagingDir}"
        mkdir -p $configStagingDir

        extractConfigRequestDir=$(grep "extractConfigRequestDir" $HE_DIR/etc/com.healthedge.customer.generic.configuration.cfg | sed -e "s/extractConfigRequestDir=//g")
        echo "extractConfigRequestDir: ${extractConfigRequestDir}"
        mkdir -p $extractConfigRequestDir

        jobName=$(grep "jobName" $bobcobConfigFile | sed -e "s/jobName//g;s/[\", :]//g")
        
        # "s/-/\./g"           - replace '-' with '.'
        # "s/\(.*\)/\L\1/g"    - convert to lower case
        configJobName=$(sed -e "s/-/\./g;s/\(.*\)/\L\1/g" <<< "$jobName")
        
        cfgFile=$HE_DIR/etc/com.healthedge.customer.generic.extract.${configJobName}.cfg
        if [ ! -f "$cfgFile" ]
        then
            echo "Dropping bcbsne_bobcob job configuration json ${bobcobConfigFile} to ${extractConfigRequestDir}"
            cp $bobcobConfigFile $extractConfigRequestDir/bcbsne-bobcob-job-config.json
        else
            echo "${jobName} is already configured"
        fi

    echo "End configuring  bcbsne_bobcob job"
    
echo "********** BCBSNE SH : bcbsne_bobcobconfigure.sh end on ${HOST_NAME} **********"
echo ""
