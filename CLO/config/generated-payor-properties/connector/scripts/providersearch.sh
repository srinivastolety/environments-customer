#!/bin/sh
#######################################################################################
# Script to copy, configure the blueprints of providersearch
# Invoke this script in post_custom_install.sh to finish up the configuration for providersearch
# those should happen post installation of the provider search features
# Ex:   Copying blueprints to HealthRulesConnectorServer
#       Configuring the frequency
#       Sets the last runtime date in ElasticSearch
#       Creates indices in Elasticsearch

# NOTE: This script does not initiate the bootstap for feeding Payor OLTP data into Elasticsearch
#######################################################################################
PROPERTY_FILE=./providersearch-jobs.properties

printUsage() {
    echo "NAME"
    echo "   ${0} - Copies providersearch specific blueprints and configures them "
    echo ""
    echo "DESCRIPTION"
    echo "    The script copies the blueprints of providersearch, configures the blueprints and sets the last entry date in Elasticsearch."
    echo "    The script requires ssh login without password setup to ConnectorServer from within the Connector VM."
    echo ""
    echo "EXAMPLE"
    echo "     ${0} <HealthRulesServerInstallationLocation> <DistributionLocation> <ConnectorUser> <ConnectorHostName>"
    echo ""
}
getScriptDir() {
    local script_name=$1
    local script_dir=`dirname $script_name`
    if [[ "X${script_dir}" = "X." ]]; then
        script_dir=`pwd`
    fi
    echo $script_dir
}

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $SCRIPT_DIR/$PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

validateArgs() {
    num_arg=$1
    if [[ $num_arg -ne 4 ]]; then
        echo "ERROR: Missing required parameters"
        printUsage
        return 100;
    elif [[ "X$arg1" = "X-h" ]]; then
        printUsage
        return 102
    fi
}


validateArgs $#
returnValidateArgs=$?
if [ $returnValidateArgs -ne 0 ]; then
   exit $returnValidateArgs
fi

SCRIPT_DIR=`getScriptDir $0`

KARAF_DIR=$1
HE_DIR=$2
HRC_USER=$3
HRC_HOST=$4
BLUEPRINTS=$HE_DIR/blueprints/providersearch
LAST_RUN_JSON=$KARAF_DIR/template/last-run-schema
CONN_BLUEPRINTS_DIR=$KARAF_DIR/connector-blueprints-deploy

if [ ! -d "$KARAF_DIR" ]; then
          echo
  	  echo "Directory $KARAF_DIR does not exist!"
  	  echo
  	  exit 0
fi
if [ ! -d "$HE_DIR" ]; then
          echo
  	  echo "Directory $HE_DIR does not exist!"
  	  echo
  	  exit 0
fi


source $PROPERTY_FILE

if [ ! -d "${LAST_RUN_JSON}_original" ]; then
	mkdir ${LAST_RUN_JSON}_original
	cp $LAST_RUN_JSON/* ${LAST_RUN_JSON}_original/
else
	cp ${LAST_RUN_JSON}_original/* ${LAST_RUN_JSON}/
fi	

#Default -Do Not change
FREQUENCY="0\+0\+23\+\?\+\*\+MON\-FRI\+2040\-2060"

cp -f $BLUEPRINTS/*.xml $KARAF_DIR/connector-blueprints-deploy

sed -i -- "s/#protocol#/$(getProperty elastic_protocol)/g" $LAST_RUN_JSON/*.json
sed -i -- "s/#host#/$(getProperty elastic_host)/g" $LAST_RUN_JSON/*.json
sed -i -- "s/#port#/$(getProperty elastic_port)/g" $LAST_RUN_JSON/*.json

sed -i -r -e "s/\"${FREQUENCY}\"/\"$(getProperty practitionerRoles_frequency)\"\ /" $CONN_BLUEPRINTS_DIR/FeedUpdatedPractitionerRoles.xml
sed -i -- "s/#starttime#/$(getProperty practitionerRoles_starttime)/g" $LAST_RUN_JSON/feedUpdatedPractitionerRoles-last-run.json
sed -i -- "s/#endtime#/$(getProperty practitionerRoles_endtime)/g" $LAST_RUN_JSON/feedUpdatedPractitionerRoles-last-run.json

sed -i -r -e "s/\"${FREQUENCY}\"/\"$(getProperty practitionerRolesForUpdatedPractitioner_frequency)\"\ /" $CONN_BLUEPRINTS_DIR/FeedPractitionerRolesForUpdatedPractitioner.xml
sed -i -- "s/#starttime#/$(getProperty practitionerRolesForUpdatedPractitioner_starttime)/g" $LAST_RUN_JSON/feedPractitionerRolesForUpdatedPractitioner-last-run.json
sed -i -- "s/#endtime#/$(getProperty practitionerRolesForUpdatedPractitioner_endtime)/g" $LAST_RUN_JSON/feedPractitionerRolesForUpdatedPractitioner-last-run.json

sed -i -r -e "s/\"${FREQUENCY}\"/\"$(getProperty practitionerRolesForUpdatedSupplier_frequency)\"\ /" $CONN_BLUEPRINTS_DIR/FeedPractitionerRolesForUpdatedSupplier.xml
sed -i -- "s/#starttime#/$(getProperty practitionerRolesForUpdatedSupplier_starttime)/g" $LAST_RUN_JSON/feedPractitionerRolesForUpdatedSupplier-last-run.json
sed -i -- "s/#endtime#/$(getProperty practitionerRolesForUpdatedSupplier_endtime)/g" $LAST_RUN_JSON/feedPractitionerRolesForUpdatedSupplier-last-run.json

sed -i -r -e "s/\"${FREQUENCY}\"/\"$(getProperty practitionerRolesForUpdatedSupplierLocation_frequency)\"\ /" $CONN_BLUEPRINTS_DIR/FeedPractitionerRolesForUpdatedSupplierLocation.xml
sed -i -- "s/#starttime#/$(getProperty practitionerRolesForUpdatedSupplierLocation_starttime)/g" $LAST_RUN_JSON/feedPractitionerRolesForUpdatedSupplierLocation-last-run.json
sed -i -- "s/#endtime#/$(getProperty practitionerRolesForUpdatedSupplierLocation_endtime)/g" $LAST_RUN_JSON/feedPractitionerRolesForUpdatedSupplierLocation-last-run.json

sed -i -r -e "s/\"${FREQUENCY}\"/\"$(getProperty supplierLocations_frequency)\"\ /" $CONN_BLUEPRINTS_DIR/FeedUpdatedSupplierLocations.xml
sed -i -- "s/#starttime#/$(getProperty supplierLocations_starttime)/g" $LAST_RUN_JSON/feedUpdatedSupplierLocations-last-run.json
sed -i -- "s/#endtime#/$(getProperty supplierLocations_endtime)/g" $LAST_RUN_JSON/feedUpdatedSupplierLocations-last-run.json

sed -i -r -e "s/\"${FREQUENCY}\"/\"$(getProperty supplierLocationsForUpdatedSupplier_frequency)\"\ /" $CONN_BLUEPRINTS_DIR/FeedSupplierLocationsForUpdatedSupplier.xml
sed -i -- "s/#starttime#/$(getProperty supplierLocationsForUpdatedSupplier_starttime)/g" $LAST_RUN_JSON/feedSupplierLocationsForUpdatedSupplier-last-run.json
sed -i -- "s/#endtime#/$(getProperty supplierLocationsForUpdatedSupplier_endtime)/g" $LAST_RUN_JSON/feedSupplierLocationsForUpdatedSupplier-last-run.json

sleep 20s

cp $KARAF_DIR/template/practitioner-role-schema/*.json $KARAF_DIR/provider-search/practitioner-role-schema
sleep 10s
cp $KARAF_DIR/template/supplier-location-schema/*.json $KARAF_DIR/provider-search/supplier-location-schema
sleep 5s
cp $KARAF_DIR/template/specialty-schema/*.json $KARAF_DIR/provider-search/specialty-schema
sleep 3s

ssh -o BatchMode=yes -oPasswordAuthentication=no -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 8101 $HRC_USER@$HRC_HOST hrc:last-run-entry file:$LAST_RUN_JSON/feedPractitionerRolesForUpdatedPractitioner-last-run.json

ssh -o BatchMode=yes -oPasswordAuthentication=no -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 8101 $HRC_USER@$HRC_HOST hrc:last-run-entry file:$LAST_RUN_JSON/feedPractitionerRolesForUpdatedSupplier-last-run.json

ssh -o BatchMode=yes -oPasswordAuthentication=no -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 8101 $HRC_USER@$HRC_HOST hrc:last-run-entry file:$LAST_RUN_JSON/feedPractitionerRolesForUpdatedSupplierLocation-last-run.json

ssh -o BatchMode=yes -oPasswordAuthentication=no -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 8101 $HRC_USER@$HRC_HOST hrc:last-run-entry file:$LAST_RUN_JSON/feedUpdatedPractitionerRoles-last-run.json

ssh -o BatchMode=yes -oPasswordAuthentication=no -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 8101 $HRC_USER@$HRC_HOST hrc:last-run-entry file:$LAST_RUN_JSON/feedUpdatedSupplierLocations-last-run.json

ssh -o BatchMode=yes -oPasswordAuthentication=no -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 8101 $HRC_USER@$HRC_HOST hrc:last-run-entry file:$LAST_RUN_JSON/feedSupplierLocationsForUpdatedSupplier-last-run.json



